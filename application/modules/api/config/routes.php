<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['api/login']['post']         					= 'api/auth/login';
$route['api/logout']['post']         					= 'api/auth/logout';
$route['api/change_password']['post']                   = 'api/auth/update_password';

$route['/api/verifikasi/history']['post']               = '/api/verifikasi/history';
$route['/api/verifikasi/detail_history']['get']         = '/api/verifikasi/detail_history';
$route['/api/verifikasi/cari_nik']['post']              = '/api/verifikasi/cari_nik';
$route['/api/verifikasi/save_brm']['post']              = '/api/verifikasi/save_brm';
$route['/api/verifikasi/save_bck']['post']              = '/api/verifikasi/save_bck';
$route['/api/verifikasi/save_kba']['post']              = '/api/verifikasi/save_kba';

$route['/api/verifikasi/upload_img']['post']            = '/api/verifikasi/upload_img';

$route['/api/verifikasi/survey_brm']['post']            = '/api/verifikasi/survey_brm';
$route['/api/verifikasi/survey_bck']['post']            = '/api/verifikasi/survey_bck';
$route['/api/verifikasi/survey_kba']['post']            = '/api/verifikasi/survey_kba';

$route['/api/verifikasi/detail_survey_brm']['post']     = '/api/verifikasi/detail_survey_brm';
$route['/api/verifikasi/detail_survey_bck']['post']     = '/api/verifikasi/detail_survey_bck';
$route['/api/verifikasi/detail_survey_kba']['post']     = '/api/verifikasi/detail_survey_kba';
$route['/api/verifikasi/verifikasi_images']['get']      = '/api/verifikasi/verifikasi_images';
$route['/api/verifikasi/keterangan']['get']             = '/api/verifikasi/keterangan';

$route['/api/pemutakhiran/data_pemutakhiran']['post']   = '/api/pemutakhiran/data_pemutakhiran';
$route['/api/pemutakhiran/data_pendidikan']['get']      = '/api/pemutakhiran/data_pendidikan';
$route['/api/pemutakhiran/data_agama']['get']           = '/api/pemutakhiran/data_agama';
$route['/api/pemutakhiran/data_statusperkawinan']['get']= '/api/pemutakhiran/data_statusperkawinan';
$route['/api/pemutakhiran/data_pekerjaan']['get']       = '/api/pemutakhiran/data_pekerjaan';
$route['/api/pemutakhiran/save_pemutakhiran']['post']   = '/api/pemutakhiran/save_pemutakhiran';
$route['/api/pemutakhiran/upload_img']['post']          = '/api/pemutakhiran/upload_img';
$route['/api/pemutakhiran/detail_pemutakhiran']['post'] = '/api/pemutakhiran/detail_pemutakhiran';
$route['/api/pemutakhiran/pemutakhiran_images']['get']  = '/api/pemutakhiran/pemutakhiran_images';
