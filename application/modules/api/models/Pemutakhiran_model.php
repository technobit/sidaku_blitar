<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Pemutakhiran_model extends MY_Model {

    public function data_pemutakhiran($user_id){
        return $this->callProcedure("mobile_get_datapemutakhiran(?)", [$user_id], 'result');
    }

    public function data_pendidikan(){
        return $this->callProcedure("mobile_get_datapendidikan()", '', 'result');
    }

    public function data_agama(){
        return $this->callProcedure("mobile_get_dataagama()", '', 'result');
    }

    public function data_statusperkawinan(){
        return $this->callProcedure("mobile_get_datastatusperkawinan()", '', 'result');
    }

    public function data_pekerjaan(){
        return $this->callProcedure("mobile_get_datapekerjaan()", '', 'result');
    }

    public function data_kota_kab(){
        return $this->callProcedure("mobile_get_datakota_kab()", '', 'result');
    }
    
    public function save_pemutakhiran($ins){
        return $this->callProcedure("mobile_pemutakhiran_insert(?,?,?, ?,?,?, ?,?,?, ?,?,?, ?,?,?, ?,?,?)", 
                                        [$ins['user_id'], 
                                        $ins['reg_no'],
                                        $ins['nik'],
                                        $ins['nama'],
                                        $ins['nohp'],
                                        $ins['pendidikan_id'],
                                        $ins['ket_pendidikan'],
                                        $ins['agama_id'],
                                        $ins['ket_agama'],
                                        $ins['pekerjaan_id'],
                                        $ins['ket_pekerjaan'],
                                        $ins['status_kawin_id'],
                                        $ins['ket_status_kawin'],
                                        $ins['tempat_lahir'],
                                        $ins['ket_tempat_lahir'],
                                        $ins['tanggal_lahir'],
                                        $ins['ket_tanggal_lahir'],
                                        $ins['periode_id']]);
    }

    public function upload_img($ins, $image){
		$this->db->trans_begin();
        
        $this->callProcedure("mobile_pemutakhiran_upload(?,?,?)", 
                            [
                            $ins['pemutakhiran_id'], 
                            $ins['jenis_img'],
                            $image['dir'].'/'.$image['file_name']
                            ]);
		$this->setQueryLog('insert_image');

        if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
    }

    public function detail_pemutakhiran($user_id, $int_pemutakhiran){
        return $this->callProcedure("mobile_get_pemutakhiran_detail(?,?)", [$user_id, $int_pemutakhiran]);
    }

    public function images($pemutakhiran_id){
        return $this->callProcedure("mobile_get_pemutakhiran_images(?,?)", [$pemutakhiran_id, $this->config->item('cdn_url')], 'result');
    }
}
