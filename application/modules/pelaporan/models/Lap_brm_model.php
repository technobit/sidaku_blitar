<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Lap_brm_model extends MY_Model {

    public function list($periode_filter = "", $rekam_filter = "", $user_filter = "", $filter_start = null, $filter_end = null, $filter = NULL, $order_by = 0, $sort = 'ASC', $limit = 0, $ofset = 0){
		$this->db->select("tvb.*, mvk.*, mk.*, ml.*,
						mlp.var_lokasi_perekaman, mkt.var_keterangan as keterangan, CONCAT(suc.txt_nama_depan, ' ', suc.txt_nama_belakang) as created,
						CONCAT(suu.txt_nama_depan, ' ', suu.txt_nama_belakang) as updated")
					->from($this->t_verifikasi_brm." tvb")
					->join($this->m_verifikasi_ktp." mvk", "tvb.var_nik = mvk.var_nik")
					->join($this->m_kecamatan." mk", "mvk.int_kecamatan_id = mk.int_kecamatan_id", "left")
					->join($this->m_kelurahan." ml", "(mvk.`int_kecamatan_id` = ml.`int_kecamatan_id` AND mvk.`int_kelurahan_id` = ml.`int_kelurahan_id`)", "left")
					->join($this->m_lokasi_perekaman." mlp", "tvb.int_lokasi_perekaman_id = mlp.int_lokasi_perekaman_id", "left")
					->join($this->m_keterangan." mkt", "tvb.int_keterangan_id = mkt.int_keterangan_id", "left")
					->join($this->s_user." suc", "tvb.created_by = suc.int_id_user", "left")
					->join($this->s_user." suu", "tvb.updated_by = suu.int_id_user", "left");;


		if($periode_filter != ""){ // filter
			$this->db->where('mvk.int_periode_id', $periode_filter);
		}
			
		if(!empty($rekam_filter)){ // filters
			$this->db->where('tvb.int_jawaban_1', $rekam_filter);
		}

		if(!empty($user_filter)){ // filters
			$this->db->where('tvb.created_by', $user_filter);
		}

		if(!empty($filter_start) && !empty($filter_end)){
			$this->whereBetweenDate('created_at', $filter_start, $filter_end);
		}

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('tvb.var_nik', $filter)
					->or_like('tvb.var_nama', $filter)
					->group_end();
		}

		$order = 'tvb.var_nik ';
		switch($order_by){
			case 1 : $order = 'tvb.var_nik '; break;
			case 2 : $order = 'tvb.var_nama '; break;
		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($periode_filter = "", $rekam_filter = "", $user_filter = "", $filter_start = null, $filter_end = null, $filter = NULL){
		$this->db->from($this->t_verifikasi_brm." tvb")
					->join($this->m_verifikasi_ktp." mvk", "tvb.var_nik = mvk.var_nik");


		if($periode_filter != ""){ // filter
			$this->db->where('mvk.int_periode_id', $periode_filter);
		}
			
		if(!empty($rekam_filter)){ // filters
			$this->db->where('tvb.int_jawaban_1', $rekam_filter);
		}

		if(!empty($user_filter)){ // filters
			$this->db->where('tvb.created_by', $user_filter);
		}

		if(!empty($filter_start) && !empty($filter_end)){
			$this->whereBetweenDate('created_at', $filter_start, $filter_end);
		}

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
            $this->db->group_start()
					->like('mvk.var_nik', $filter)
					->or_like('mvk.var_nama', $filter)
					->group_end();
        }
		return $this->db->count_all_results();
	}

	public function get_lampiran_export($int_verifikasi_brm_id){
		return $this->db->query("SELECT *
								FROM  {$this->t_verifikasi_brm_img} 
								WHERE int_verifikasi_brm_id = {$int_verifikasi_brm_id}")->result();
			
	}
}
