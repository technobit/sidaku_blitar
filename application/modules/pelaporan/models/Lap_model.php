<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Lap_model extends MY_Model {

	public function list_user(){
		$this->db->select("int_id_user, CONCAT(txt_nama_depan, ' ', txt_nama_belakang) as nama")
					->from($this->s_user);

		if($this->session->userdata['group_id'] != 1){ // filters 
			$this->db->where('int_id_user', $this->session->userdata['user_id']);
		}

		$order = 'txt_nama_depan ';
		$sort = 'ASC';
		
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
}
