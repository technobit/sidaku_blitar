<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Lap_brm extends MX_Controller {
	private $input_file_name = 'lampiran';

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'LAP-BRM'; // kode data pada tabel data, 1 data : 1 controller
		$this->module   = 'pelaporan';
		$this->routeURL = 'lap_brm';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('lap_brm_model', 'model');
		$this->load->model('lap_model', 'lap');
		$this->load->model('master/periode_model', 'periode');

    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Pelaporan Belum Rekam KTP';
		$this->page->menu 	  = 'pelaporan';
		$this->page->submenu1 = 'lap_verifikasi';
		$this->page->submenu2 = 'lap_brm';
		$this->breadcrumb->title = 'Pelaporan Verifikasi Data KTP';
		$this->breadcrumb->card_title = 'Belum Rekam KTP';
		$this->breadcrumb->icon = 'fas fa-camera-retro';
		$this->breadcrumb->list = ['Pelaporan', 'Verifikasi Data KTP', 'Belum Rekam'];
		$this->css = true;
		$this->js = true;
		$data['periode']		= $this->periode->get_periode();
		$data['list_user'] = $this->lap->list_user();
		$data['url'] = site_url("{$this->routeURL}/add");
		$data['url_export'] = site_url("export/{$this->routeURL}");
		$this->render_view('lap_brm/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r'); 

        if(strpos($this->input->post('date_filter', true), '~') !== false){
            list($filter_start, $filter_end) = explode(' ~ ', $this->input->post('date_filter', true));
        }

		$filter_start = convertValidDate($filter_start, 'd-m-Y', 'Y-m-d').' 00:00:00';
		if($filter_start == '0001-01-01 00:00:00'){$filter_start = null;}
        $filter_end	  = convertValidDate($filter_end, 'd-m-Y', 'Y-m-d').' 23:59:59';
		if($filter_end == '9999-12-31 23:59:59'){$filter_end = null;}

		$data  = array();
		$total = $this->model->listCount($this->input->post('periode_filter', true), $this->input->post('rekam_filter', true), $this->input->post('user_filter', true), $filter_start, $filter_end, $this->input_post('search[value]', TRUE));
		$ldata = $this->model->list($this->input->post('periode_filter', true), $this->input->post('rekam_filter', true), $this->input->post('user_filter', true), $filter_start, $filter_end, $this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

		$i 	   = $this->input_post('start', true);
		foreach($ldata as $d){
			$i++;
			if($d->int_jawaban_2 == 0){
				$lokasi = $d->var_lokasi_perekaman;
			}else if($d->int_jawaban_2 == 1){
				$lokasi = 'Kota Blitar';
			}else if($d->int_jawaban_2 == 2){
				$lokasi = 'Luar Kota';
			}

			if($d->int_jawaban_1 == 1){
				$dt_perekaman = idn_date($d->dt_perekaman, 'j F Y');
			}else{
				$dt_perekaman = '';
			}
			$data[] = array($i, $d->var_nik, $d->var_nama, $d->var_nohp, $d->int_jawaban_1, $lokasi, $dt_perekaman, $d->int_jawaban_3, idn_date($d->created_at), $d->created);
		}
		$this->set_json(array( 'stat' => TRUE,
								'iTotalRecords' => $total,
								'iTotalDisplayRecords' => $total,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()),200,false);
	}

	public function export(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page
		
		$txt_rekam = $this->input->post('rekam_txt');
		$txt_user = $this->input->post('user_txt');
        $filename = 'Laporan BRM - '.$txt_rekam.' - '.$txt_user.' - '.idn_date(date("Y-M-d H:i:s"), 'j F Y H.i.s').'.xlsx';
        $title    = 'BRM';

        /*list($start, $end) = explode(' ~ ', $this->input->post('tanggal', true));
        $filter_start = convertValidDate($start, 'd-m-Y', 'Y-m-d');
        $filter_end	  = convertValidDate($end, 'd-m-Y', 'Y-m-d');*/

        //$ldata = $this->report->list($filter_start, $filter_end, $is_order, $this->input->post('search_key', TRUE));
		$ldata = $this->model->list($this->input->post('rekam_filter', true), $this->input->post('user_filter', true), $this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true), true);

        $judul = 'Laporan Belum Rekam KTP';

        $spreadsheet = new Spreadsheet();
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A1', $judul)
            ->setCellValue('A2', 'Tanggal:')
            ->setCellValue('C2', idn_date(date("Y-M-d")))
            ->setCellValue('A3', 'No')
            ->setCellValue('B3', 'NIK')
            ->setCellValue('C3', 'NAMA')
            ->setCellValue('D3', 'KELURAHAN')
            ->setCellValue('E3', 'KECAMATAN')
            ->setCellValue('F3', 'NO HP')
            ->setCellValue('G3', 'PEREKAMAN')
            ->setCellValue('G4', 'STATUS')
            ->setCellValue('H4', 'LOKASI')
            ->setCellValue('I4', 'TANGGAL REKAM')
            ->setCellValue('J4', 'KEPEMILIKAN')
            ->setCellValue('K3', 'KETERANGAN')
            ->setCellValue('M3', 'PETUGAS')
            ->setCellValue('N3', 'FOTO');

        $sheet = $spreadsheet->getActiveSheet();
        $sheet->mergeCells('A1:L1');
        $sheet->mergeCells('A3:A4');
        $sheet->mergeCells('B3:B4');
        $sheet->mergeCells('C3:C4');
        $sheet->mergeCells('D3:D4');
        $sheet->mergeCells('E3:E4');
        $sheet->mergeCells('F3:F4');
        $sheet->mergeCells('K3:L4');
        $sheet->mergeCells('M3:M4');
        $sheet->mergeCells('N3:N4');
        $sheet->mergeCells('G3:J3');
        $sheet->getStyle('A1')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A3:N4')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A3:N4')->getAlignment()->setVertical('center');
        $sheet->getStyle('A1')->getFont()->setBold( true );
        $sheet->getStyle('A2')->getFont()->setBold( true );
        $sheet->getStyle('A3:N4')->getFont()->setBold( true );
        $sheet->getStyle('A4:N4')->getFont()->setBold( true );
        $sheet->getStyle('A3:N4')->getFill()->setFillType('solid')->getStartColor()->setARGB('f2f2f2');
        $sheet->getStyle('G3:J4')->getFill()->setFillType('solid')->getStartColor()->setARGB('dbedc7');

		$i = 0;
        $x = 4;
        $total = 0;
        $tra   = 0;
        foreach($ldata as $d){
			$get_lampiran_export = $this->model->get_lampiran_export($d->int_verifikasi_brm_id);
			$lampiran = '';		
			foreach($get_lampiran_export as $lmp){
				$lampiran .= cdn_url().$lmp->var_image."\n";
			}

            $i++;
			$x++;

			switch ($d->int_jawaban_1) {
				case 1:
					$perekaman = 'Belum'; break;
				case 2:
					$perekaman = 'Sudah';  break;
				default:
					$perekaman = '';
			} 

			switch ($d->int_jawaban_2) {
				case 0:
					$lokasi = $d->var_lokasi_perekaman; break;
				case 1:
					$lokasi = 'Kota Blitar'; break;
				case 2:
					$lokasi = 'Luar Kota';  break;
				default:
					$lokasi = '';
			} 

			switch ($d->int_jawaban_3) {
				case 1:
					$kepemilikan = 'Belum'; break;
				case 2:
					$kepemilikan = 'Sudah';  break;
				default:
					$kepemilikan = '';
			} 

			$sheet->setCellValue('A'.$x, $i);
			$sheet->setCellValueExplicit('B'.$x, $d->var_nik,\PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $sheet->setCellValue('C'.$x, $d->var_nama);
            $sheet->setCellValue('D'.$x, $d->var_kelurahan);
            $sheet->setCellValue('E'.$x, $d->var_kecamatan);
			$sheet->setCellValueExplicit('F'.$x, $d->var_nohp,\PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $sheet->setCellValue('G'.$x, $perekaman);
            $sheet->setCellValue('H'.$x, $lokasi);
            $sheet->setCellValue('I'.$x, idn_date($d->dt_perekaman, 'j F Y'));
            $sheet->setCellValue('J'.$x, $kepemilikan);
            $sheet->setCellValue('K'.$x, $d->keterangan);
            $sheet->setCellValue('L'.$x, $d->var_keterangan);
            $sheet->setCellValue('M'.$x, $d->created);
			$sheet->setCellValue('N'.$x, $lampiran);

			$sheet->getStyle('K'.$x)->getAlignment()->setWrapText(true);
			$sheet->getStyle('L'.$x)->getAlignment()->setWrapText(true);
			$sheet->getStyle('N'.$x)->getAlignment()->setWrapText(true);
			$sheet->getStyle('A'.$x.':M'.$x.'')->getAlignment()->setVertical('top');
			$sheet->getStyle('A'.$x)->getAlignment()->setHorizontal('right');
			$sheet->getStyle('B'.$x)->getAlignment()->setHorizontal('right');
			$sheet->getStyle('F'.$x)->getAlignment()->setHorizontal('right');
			$sheet->getStyle('I'.$x)->getAlignment()->setHorizontal('right');
		}
        $i++;
        $x++;

        foreach(range('A','N') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }
		$sheet->getColumnDimension('K')->setAutoSize(false);
		$sheet->getColumnDimension('K')->setWidth(30);
		$sheet->getColumnDimension('L')->setAutoSize(false);
		$sheet->getColumnDimension('L')->setWidth(40);
		$sheet->getColumnDimension('N')->setAutoSize(false);
		$sheet->getColumnDimension('N')->setWidth(40);

        $sheet->setTitle($title);
        $spreadsheet->setActiveSheetIndex(0);

        $this->set_header($filename);

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		if (ob_get_contents()) ob_end_clean();
		$writer->save('php://output');
		exit;
    }
}
