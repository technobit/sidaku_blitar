<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Lap_kba extends MX_Controller {
	private $input_file_name = 'lampiran';

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'LAP-KBA'; // kode data pada tabel data, 1 data : 1 controller
		$this->module   = 'pelaporan';
		$this->routeURL = 'lap_kba';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('lap_kba_model', 'model');
		$this->load->model('lap_model', 'lap');
		$this->load->model('master/periode_model', 'periode');

    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Pelaporan Belum Ambil KTP';
		$this->page->menu 	  = 'pelaporan';
		$this->page->submenu1 = 'lap_verifikasi';
		$this->page->submenu2 = 'lap_kba';
		$this->breadcrumb->title = 'Pelaporan Verifikasi Data KTP';
		$this->breadcrumb->card_title = 'Belum Ambil KTP';
		$this->breadcrumb->icon = 'fas fa-hand-holding';
		$this->breadcrumb->list = ['Pelaporan', 'Verifikasi Data KTP', 'Belum Ambil'];
		$this->css = true;
		$this->js = true;
		$data['periode']		= $this->periode->get_periode();
		$data['all_user'] = '';
		$data['list_user'] = $this->lap->list_user();
		$data['url'] = site_url("{$this->routeURL}/add");
		$data['url_export'] = site_url("export/{$this->routeURL}");
		$this->render_view('lap_kba/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r'); 

        if(strpos($this->input->post('date_filter', true), '~') !== false){
            list($filter_start, $filter_end) = explode(' ~ ', $this->input->post('date_filter', true));
        }

		$filter_start = convertValidDate($filter_start, 'd-m-Y', 'Y-m-d').' 00:00:00';
		if($filter_start == '0001-01-01 00:00:00'){$filter_start = null;}
        $filter_end	  = convertValidDate($filter_end, 'd-m-Y', 'Y-m-d').' 23:59:59';
		if($filter_end == '9999-12-31 23:59:59'){$filter_end = null;}

		$data  = array();
		$total = $this->model->listCount($this->input->post('periode_filter', true), $this->input->post('ambil_filter', true), $this->input->post('user_filter', true), $filter_start, $filter_end, $this->input_post('search[value]', TRUE));
		$ldata = $this->model->list($this->input->post('periode_filter', true), $this->input->post('ambil_filter', true), $this->input->post('user_filter', true), $filter_start, $filter_end, $this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

		$i 	   = $this->input_post('start', true);
		foreach($ldata as $d){
			$i++;
			$data[] = array($i, $d->var_nik, $d->var_nama, $d->var_nohp, $d->int_jawaban_1, idn_date($d->created_at), $d->created);
		}
		$this->set_json(array( 'stat' => TRUE,
								'iTotalRecords' => $total,
								'iTotalDisplayRecords' => $total,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()),200,false);
	}

	public function export(){
        $this->authCheckDetailAccess('r'); // hak akses untuk render page

		
		$txt_ambil = $this->input->post('ambil_txt');
		$txt_user = $this->input->post('user_txt');
        $filename = 'Laporan KBA - '.$txt_ambil.' - '.$txt_user.' - '.idn_date(date("Y-M-d H:i:s"), 'j F Y H.i.s').'.xlsx';
        $title    = 'KBA';

        /*list($start, $end) = explode(' ~ ', $this->input->post('tanggal', true));
        $filter_start = convertValidDate($start, 'd-m-Y', 'Y-m-d');
        $filter_end	  = convertValidDate($end, 'd-m-Y', 'Y-m-d');*/

		$ldata = $this->model->list($this->input->post('ambil_filter', true), $this->input->post('user_filter', true), $this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

        $judul = 'Laporan Belum Ambil KTP';

        $spreadsheet = new Spreadsheet();
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A1', $judul)
            ->setCellValue('A3', 'Tanggal:')
            ->setCellValue('C3', idn_date(date("Y-M-d")))
            ->setCellValue('A4', 'No')
            ->setCellValue('B4', 'NIK')
            ->setCellValue('C4', 'NAMA')
            ->setCellValue('D4', 'KELURAHAN')
            ->setCellValue('E4', 'KECAMATAN')
            ->setCellValue('F4', 'NO HP')
            ->setCellValue('G4', 'STATUS KTP')
            ->setCellValue('H4', 'KETERANGAN')
            ->setCellValue('J4', 'PETUGAS')
            ->setCellValue('K4', 'FOTO');

        $sheet = $spreadsheet->getActiveSheet();
        $sheet->mergeCells('A1:K1');
        $sheet->mergeCells('H4:I4');
        $sheet->getStyle('A1')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A4:K4')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A1')->getFont()->setBold( true );
        $sheet->getStyle('A3')->getFont()->setBold( true );
        $sheet->getStyle('A4:K4')->getFont()->setBold( true );
        $sheet->getStyle('A4:K4')->getFill()->setFillType('solid')->getStartColor()->setARGB('f2f2f2');

        $i = 0;
        $x = 4;
        $total = 0;
        $tra   = 0;
        foreach($ldata as $d){
			$get_lampiran_export = $this->model->get_lampiran_export($d->int_verifikasi_kba_id);
			$lampiran = '';		
			foreach($get_lampiran_export as $lmp){
				$lampiran .= cdn_url().$lmp->var_image."\n";
			}
            $i++;
			$x++;

			switch ($d->int_jawaban_1) {
				case 1:
					$ktp = 'Belum'; break;
				case 2:
					$ktp = 'Sudah';  break;
				default:
					$ktp = '';
			}

            $sheet->setCellValue('A'.$x, $i);
			$sheet->setCellValueExplicit('B'.$x, $d->var_nik,\PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $sheet->setCellValue('C'.$x, $d->var_nama);
            $sheet->setCellValue('D'.$x, $d->var_kelurahan);
            $sheet->setCellValue('E'.$x, $d->var_kecamatan);
			$sheet->setCellValueExplicit('F'.$x, $d->var_nohp,\PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $sheet->setCellValue('G'.$x, $ktp);
            $sheet->setCellValue('H'.$x, $d->keterangan);
            $sheet->setCellValue('I'.$x, $d->var_keterangan);
			$sheet->setCellValue('J'.$x, $d->created);
			$sheet->setCellValue('K'.$x, $lampiran);
			
			$sheet->getStyle('H'.$x)->getAlignment()->setWrapText(true);
			$sheet->getStyle('I'.$x)->getAlignment()->setWrapText(true);
			$sheet->getStyle('K'.$x)->getAlignment()->setWrapText(true);
			$sheet->getStyle('A'.$x.':J'.$x.'')->getAlignment()->setVertical('top');
			$sheet->getStyle('A'.$x)->getAlignment()->setHorizontal('right');
			$sheet->getStyle('B'.$x)->getAlignment()->setHorizontal('right');
			$sheet->getStyle('F'.$x)->getAlignment()->setHorizontal('right');

        }
        $i++;
        $x++;

        $sheet->getStyle('A'.$x)->getAlignment()->setHorizontal('right');

        foreach(range('A','K') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }
		$sheet->getColumnDimension('H')->setAutoSize(false);
		$sheet->getColumnDimension('H')->setWidth(30);
		$sheet->getColumnDimension('I')->setAutoSize(false);
		$sheet->getColumnDimension('I')->setWidth(40);
		$sheet->getColumnDimension('K')->setAutoSize(false);
		$sheet->getColumnDimension('K')->setWidth(40);

        $sheet->setTitle($title);
        $spreadsheet->setActiveSheetIndex(0);

        $this->set_header($filename);

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		if (ob_get_contents()) ob_end_clean();
		$writer->save('php://output');
		exit;
    }

}
