<div class="container-fluid">
    <div class="row">
        <section class="col-lg-12">
            <div class="card card-outline">
                <div class="card-header">
                    <h3 class="card-title mt-1">
                        <i class="<?=isset($breadcrumb->icon)? $breadcrumb->icon : 'far fa-circle'?>"></i>
                        <?=isset($breadcrumb->card_title)? $breadcrumb->card_title :  $breadcrumb->title?>
                    </h3>
					<div class="card-tools">
                        <button type="button" class="btn btn-sm btn-success" onclick="exportData(this)" data-block="body" data-url="<?php echo $url_export ?>" ><i class="fas fa-file-excel"></i> Export</button>
					</div>
                </div><!-- /.card-header -->
                <div class="card-body p-0">
                    <div id="filter" class="form-horizontal filter-date p-2 border-bottom">
						<div class="row">
                            <div class="col-md-6">
								<div class="form-group row text-sm mb-0">
									<label for="periode_filter" class="col-md-4 col-form-label">Periode</label>
									<div class="col-md-8">
                                        <select id="int_periode_id" name="int_periode_id" class="form-control form-control-sm periode_filter select2" style="width: 100%;">
                                            <?php 
                                                foreach($periode as $per){
                                                    echo '<option value="'.$per->int_periode_id.'">'.$per->var_periode.'</option>';
                                                }
                                            ?>
                                        </select>
									</div>
								</div>
                            </div>
							<div class="col-md-6">
								<div class="form-group row text-sm mb-0">
									<label for="cetak_filter" class="col-md-4 col-form-label">Status Kepemilikan KTP</label>
									<div class="col-md-8">
                                        <select id="cetak_filter" name="cetak_filter" class="form-control form-control-sm cetak_filter select2" style="width: 100%;">
                                            <option value="">- Semua Status -</option>
                                            <option value="1">Belum Punya</option>
                                            <option value="2">Sudah Punya</option>
                                        </select>
									</div>
								</div>
							</div>
                            <div class="col-md-6">
								<div class="form-group form-group-sm row text-sm mb-0">
									<label for="filter_date" class="col-md-4 col-form-label">Tanggal Verifikasi</label>
									<div class="col-md-8">
										<input type="text" name="date_filter" class="form-control form-control-sm date_filter">
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group row text-sm mb-0">
                                    <label for="user_filter" class="col-md-4 col-form-label">Petugas</label>
									<div class="col-md-8">
                                        <select id="user_filter" name="user_filter" class="form-control form-control-sm user_filter select2" style="width: 100%;">
                                            <?php if($this->session->userdata['group_id'] == 1){?>
                                            <option value="">- Semua Petugas - </option>
                                            <?php } ?>
                                            <?php 
                                                foreach($list_user as $lu){
                                                    echo '<option value="'.$lu->int_id_user.'">'.$lu->nama.'</option>';
                                                }
                                            ?>
                                        </select>
									</div>
								</div>
							</div>
						</div>
					</div>
                    <table class="table table-striped table-hover table-full-width" id="datatable">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>NIK</th>
                            <th>Nama</th>
                            <th>No. HP</th>
                            <th>Kepemilikan KTP</th>
                            <th>Tanggal</th>
                            <th>Petugas</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>
<div id="ajax-modal" class="modal fade animate shake" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="75%" aria-hidden="true"></div>
<div id="ajax-modal-confirm" class="modal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="50%" aria-hidden="true"></div>
