<script src="<?=base_url()?>assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script src="<?=base_url() ?>assets/plugins/lightGallery/dist/js/lightgallery-all.min.js"></script>
<script src="<?=base_url() ?>assets/plugins/lightGallery/lib/jquery.mousewheel.min.js"></script>
<script>
    function exportData(th){
        $('.form-message').html('');
        let blc = $(th).data('block');
        blockUI(blc);
        $.AjaxDownloader({
            url  : $(th).data('url'),
            data : {
                <?php echo $page->tokenName ?> : $('meta[name=<?php echo $page->tokenName ?>]').attr("content"),
                rekam_filter : $('.rekam_filter').val(),
                user_filter : $('.user_filter').val(),
                rekam_txt : $('.rekam_filter option:selected').html(),
                user_txt : $('.user_filter option:selected').html()
                //search_key: $('.dataTables_filter input').val()
            }
        });
        setTimeout(function(){unblockUI(blc)}, 3000);
    }

    var dataTable;
    $(document).ready(function() {
		$('.select2').select2();
        dataTable = $('#datatable').DataTable({
            "bFilter": true,
            "bServerSide": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "pageLength": 50,
            "lengthMenu": [[25, 50, 75, 100, -1], [25, 50, 75, 100, "All"]],
            "ajax": {
                "url": "<?=site_url("{$routeURL}") ?>",
                "dataType": "json",
                "type": "POST",
                "data": function(d) {
                    d.<?=$page->tokenName ?> = $('meta[name=<?=$page->tokenName ?>]').attr("content");
					d.date_filter = $('.date_filter').val();
					d.periode_filter = $('.periode_filter').val();
                    d.rekam_filter = $('.rekam_filter').val();
					d.user_filter = $('.user_filter').val();

                },
                "dataSrc": function(json) {
                    if (json.<?=$page->tokenName ?> !== undefined) $('meta[name=<?=$page->tokenName ?>]').attr("content", json.<?=$page->tokenName ?>);
                    return json.aaData;
                }
            },
            "aoColumns": [{
                    "sWidth": "10",
                    "class" : "text-right",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto",
                    "class" : "text-right",
                },
                {
                    "sWidth": "auto",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "auto",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "120",
                    "class" : "text-right",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "auto",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "auto",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "auto",
                    "bSearchable": false,
                    "bSortable": false
                }
            ],
            "aoColumnDefs": [{
                    "aTargets": [4],
                    "mRender": function(data, type, row, meta) {
                        switch (data) {
                            case '0':
                                return '';
                                break;
                            case '1':
                                return '<span class="badge bg-danger">Belum</span>';
                                break;
                            case '2':
                                return '<span class="badge bg-success">Sudah</span>';
                                break;
                            default: '';
                        }
                    }
                },
                {
                    "aTargets": [7],
                    "mRender": function(data, type, row, meta) {
                        switch (data) {
                            case '0':
                                return '';
                                break;
                            case '1':
                                return '<span class="badge bg-danger">Belum</span>';
                                break;
                            case '2':
                                return '<span class="badge bg-success">Sudah</span>';
                                break;
                            default: '';
                        }
                    }
                }
            ]
        });
        $('.dataTables_filter input').unbind().bind('keyup', function(e) {
			if (e.keyCode == 13) {
				dataTable.search($(this).val()).draw();
			}
        });
        
        $('.periode_filter').change(function(){
            dataTable.draw();
        });

        $('.rekam_filter').change(function(){
            dataTable.draw();
        });

        $('.user_filter').change(function(){
            dataTable.draw();
        });

    });
</script>