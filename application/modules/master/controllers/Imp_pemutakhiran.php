<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
class Imp_pemutakhiran extends MX_Controller {
	private $input_file_name = 'data_ktp';
	private $import_dir = 'assets/import';

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'IMP-PEMUTAKHIRAN'; // kode customer pada tabel customer, 1 customer : 1 controller
		$this->module   = 'master';
		$this->routeURL = 'imp_pemutakhiran';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('imp_pemutakhiran_model', 'model');
		$this->load->model('master/periode_model', 'periode');
		$this->load->model('master/wilayah_model', 'wilayah');

    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Import Data Pemutakhiran KTP';
		$this->page->menu 	  = 'master';
		$this->page->submenu1 = 'imp_pemutakhiran';
		$this->breadcrumb->title = 'Import Data Pemutakhiran KTP';
		$this->breadcrumb->card_title = 'Data Pemutakhiran KTP';
		$this->breadcrumb->icon = 'fas fa-upload';
		$this->breadcrumb->list = ['Master', 'Import Data Pemutakhiran KTP'];
		$this->js = true;
		$data['periode']      = $this->periode->get_periode();
		$data['kecamatan_usr'] = $this->session->userdata['int_kecamatan_id'];
		$data['kelurahan_usr'] = $this->session->userdata['int_kelurahan_id'];
		$data['list_kecamatan'] = $this->wilayah->get_kecamatan_usr();
		$data['url'] = site_url("{$this->routeURL}/add");
		$this->render_view('imp_pemutakhiran/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r'); 

		$data  = array();
		$total = $this->model->listCount($this->input->post('periode_filter', true), $this->input->post('kategori_filter', true), $this->input->post('kecamatan_filter', true), $this->input->post('kelurahan_filter', true), $this->input_post('search[value]', TRUE));
		$ldata = $this->model->list($this->input->post('periode_filter', true), $this->input->post('kategori_filter', true), $this->input->post('kecamatan_filter', true), $this->input->post('kelurahan_filter', true), $this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

		$i 	   = $this->input_post('start', true);
		foreach($ldata as $d){
			$i++;
			$data[] = array($i, $d->var_nik, $d->var_nama, $d->var_tempat_lahir.', '.(idn_date($d->dt_tanggal_lahir, "j F Y")), $d->var_kecamatan, $d->var_kelurahan, $d->int_mpemutakhiran_ktp_id);
		}
		$this->set_json(array( 'stat' => TRUE,
								'iTotalRecords' => $total,
								'iTotalDisplayRecords' => $total,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()));
	}

	public function add(){
		if($this->authCheckDetailAccess('c', true) == false) return; // hak akses untuk modal popup

		$data['url']        = site_url("{$this->routeURL}/save");
		$data['title']      = 'Import Data';
		$data['input_file_name'] = $this->input_file_name;
		$data['periode']      = $this->periode->get_periode();
		$this->load_view('imp_pemutakhiran/index_import', $data, true);
		
	}

	public function save(){
		$this->authCheckDetailAccess('c');
		
		$start = start_time();
		$this->form_validation->set_rules('mulai', 'Mulai', 'required|integer');
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'mc' => false, //modal close
								'msg' => "Terjadi kesalahan",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        }else{
			if(isset($_FILES[$this->input_file_name])){
				$config['upload_path']   = "./{$this->import_dir}"; 
				$config['allowed_types'] = 'xlsx|xls'; 
				$config['encrypt_name']  = true; 
				$config['max_size']      = 4096;  
				$this->load->library('upload', $config);
				
				if($this->upload->do_upload($this->input_file_name)){
					$status  = $this->model->import($this->input->post(), $this->upload->data());
					$this->set_json([  'stat' => ($status !== false), 
								'mc' => ($status !== false), //modal close
								'time' => finish_time($start),
								'msg' => ($status !== false)? "Data berhasil di-import dengan {$status} baris data." : 'Data gagal di-import',
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);
				} else {
					$this->set_json([  'stat' => false, 
										'mc' => false, //modal close
										'msg' => "Data gagal di-import. ".$this->upload->display_errors('',''),
										'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
									]);
				}
			} else {
				$this->set_json([  'stat' => false, 
									'mc' => false, //modal close
									'msg' => "Data gagal di-import",
									'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
								]);
			}

        }
	}

	public function confirm($int_mpemutakhiran_ktp_id){
		if($this->authCheckDetailAccess('d', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get($int_mpemutakhiran_ktp_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Error Detected.', 'message' => 'Data not found. ']], true);
		}else{
			$data['url']	= site_url("{$this->routeURL}/$int_mpemutakhiran_ktp_id/del");
			$data['title']	= 'Hapus Data KTP';
			$data['info']   = [ 'NIK' => $res->var_nik,
                                'Nama' => $res->var_nama,
                                'Tanggal Lahir' => $res->dt_tanggal_lahir];
			$this->load_view('imp_pemutakhiran/index_delete', $data);
		}
	}

	public function delete($int_mpemutakhiran_ktp_id){
		$this->authCheckDetailAccess('d');

		$check = $this->model->delete($int_mpemutakhiran_ktp_id);
		$this->set_json([  'stat' => $check, 
							'mc' => $check, //modal close
							'msg' => ($check)? "Data Deleted Successfully" : "Data Delete Failed",
							'csrf' => [ 'name' => $this->getCsrfName(),
										'token' => $this->getCsrfToken()]
						]);
		
	}
}
