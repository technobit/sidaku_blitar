<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
class Periode extends MX_Controller {
	private $input_file_name = 'data_ktp';
	private $import_dir = 'assets/import';

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'PERIODE'; // kode customer pada tabel customer, 1 customer : 1 controller
		$this->module   = 'master';
		$this->routeURL = 'periode';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('periode_model', 'model');
		$this->load->model('master/wilayah_model', 'wilayah');

    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Waktu Pelaksanaan Pekerjaan';
		$this->page->menu 	  = 'master';
		$this->page->submenu1 = 'periode';
		$this->breadcrumb->title = 'Waktu Pelaksanaan Pekerjaan';
		$this->breadcrumb->card_title = 'Data Waktu Pelaksanaan Pekerjaan';
		$this->breadcrumb->icon = 'fas fa-business-time';
		$this->breadcrumb->list = ['Master', 'Waktu Pelaksanaan Pekerjaan'];
		$this->js = true;
		$data['url'] = site_url("{$this->routeURL}/add");
		$this->render_view('periode/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r'); 

		$data  = array();
		$total = $this->model->listCount($this->input_post('search[value]', TRUE));
		$ldata = $this->model->list($this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

		$i 	   = $this->input_post('start', true);
		foreach($ldata as $d){
			$i++;
			$data[] = array($i, $d->var_periode, idn_date($d->dt_periode, "j F Y"), $d->int_periode_id);
		}
		$this->set_json(array( 'stat' => TRUE,
								'iTotalRecords' => $total,
								'iTotalDisplayRecords' => $total,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()));
	}

	public function add(){
		if($this->authCheckDetailAccess('c', true) == false) return; // hak akses untuk modal popup

		$data['url']        = site_url("{$this->routeURL}/save");
		$data['title']      = 'Waktu Pelaksanaan Pekerjaan';
		$data['input_file_name'] = $this->input_file_name;
		$this->load_view('periode/index_action', $data, true);
		
	}

	public function save(){
		$this->authCheckDetailAccess('u');
		
		$this->form_validation->set_rules('var_periode', 'Periode Pekerjaan', 'required');
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'msg' => "Validasi Data Gagal",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        } else {
            $check = $this->model->create( $this->input_post());
			$this->set_json([  'stat' => $check, 
								'mc'   => $check, //modal close
								'msg'  => ($check)? "Data Berhasil Diubah" : "Data Gagal Diubah",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);
        }
	}
	
	public function get($int_periode_id){
		if($this->authCheckDetailAccess('u', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get($int_periode_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Error Detected.', 'message' => 'Data not found. ']], true);
		}else{
			$data['data'] 	= $res;
			$data['url']	= site_url("{$this->routeURL}/$int_periode_id");
			$data['title']	= 'Edit Periode Pekerjaan';
			$this->load_view('periode/index_action', $data);
		}
		
	}

	public function update($int_periode_id){
		$this->authCheckDetailAccess('u');
		
		$this->form_validation->set_rules('var_periode', 'Waktu Pelaksanaan Pekerjaan', 'required');
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'msg' => "Validasi Data Gagal",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        } else {
            $check = $this->model->update($int_periode_id, $this->input_post());
			$this->set_json([  'stat' => $check, 
								'mc'   => $check, //modal close
								'msg'  => ($check)? "Data Berhasil Diubah" : "Data Gagal Diubah",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);
        }
	}

	public function confirm($int_periode_id){
		if($this->authCheckDetailAccess('d', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get($int_periode_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Error Detected.', 'message' => 'Data not found. ']], true);
		}else{
			$data['url']	= site_url("{$this->routeURL}/$int_periode_id/del");
			$data['title']	= 'Hapus Periode';
			$data['info']   = [ 'Periode Pekerjaan' => $res->var_periode,
                                'Waktu' => $res->dt_periode];
			$this->load_view('periode/index_delete', $data);
		}
	}

	public function delete($int_periode_id){
		$this->authCheckDetailAccess('d');

		$check = $this->model->delete($int_periode_id);
		$this->set_json([  'stat' => $check, 
							'mc' => $check, //modal close
							'msg' => ($check)? "Data Berhasil Dihapus" : "Data Gagal Dihapus",
							'csrf' => [ 'name' => $this->getCsrfName(),
										'token' => $this->getCsrfToken()]
						]);
		
	}
}
