<form method="post" action="<?=$url ?>" role="form" class="form-horizontal" id="action-form" width="80%">
<div id="modal-action" class="modal-dialog modal-md" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel"><?=$title?></h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="form-message text-center"></div>
			<div class="form-group row mb-1">
				<label for="var_periode" class="col-sm-4 col-form-label">Periode Pekerjaan</label>
				<div class="col-sm-8">
					<input type="text" id="var_periode" name="var_periode" class="form-control form-control-sm" value="<?=isset($data->var_periode)? $data->var_periode : ''?>">
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="dt_periode" class="col-sm-4 col-form-label">Waktu</label>
				<div class="col-sm-8">
					<input type="text" id="dt_periode" name="dt_periode" class="form-control form-control-sm date_picker" value="<?=isset($data->dt_periode)? $data->dt_periode : ''?>">
				</div>
			</div>		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-danger">Keluar</button>
			<button type="submit" class="btn btn-success">Simpan</button>
		</div>
	</div>
</div>
<?=form_close() ?>

<script>
	$(document).ready(function(){
		$('.date_picker').daterangepicker(datepickModal);
		$("#action-form").validate({
			rules: {
				var_periode: {
					required: true,
					minlength: 4,
					maxlength: 50
				},
				dt_periode:{
					required: true,
				}
			},
			submitHandler: function(form) {
				$('.form-message').html('');
				blockUI(form);
				$(form).ajaxSubmit({
					dataType:  'json',
					data: {<?=$page->tokenName ?> : $('meta[name=<?=$page->tokenName ?>]').attr("content")},
					success: function(data){
						unblockUI(form);
						setFormMessage('.form-message', data);
						if(data.stat){
							resetForm('#user-form');
							dataTable.draw();
						}
						closeModal($modal, data);
					}
				});
			},
			validClass: "valid-feedback",
			errorElement: "div", // contain the error msg in a small tag
			errorClass: 'invalid-feedback',
			errorPlacement: erp,
			highlight: hl,
			unhighlight: uhl,
			success: sc
		});
	});
</script>