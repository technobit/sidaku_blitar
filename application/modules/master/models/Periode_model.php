<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Periode_model extends MY_Model {

	public function get_periode(){
		return $this->db->query("SELECT *
								 FROM	{$this->m_periode}
								 ORDER BY dt_periode DESC")->result();
	}

	public function list($filter = NULL, $order_by = 0, $sort = 'DESC', $limit = 0, $ofset = 0){
		$this->db->select("*")
				->from($this->m_periode);

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('var_periode', $filter)
					->group_end();
		}

		$order = 'dt_periode';
		switch($order_by){
			case 1 : $order = 'var_periode'; break;
			case 2 : $order = 'dt_periode'; break;
		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($filter = NULL){
		$this->db->select("*")
				->from($this->m_periode);

		if(!empty($filter)){ // filters
			$filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('var_periode', $filter)
					->group_end();
		}
		
		return $this->db->count_all_results();
	}

	public function create($ins){
		$this->db->trans_begin();
		$this->db->insert($this->m_periode, $ins);
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function get($int_periode_id){
		return $this->db->select("*")
					->get_where($this->m_periode, ['int_periode_id' => $int_periode_id])->row();
	}

	public function update($int_periode_id, $ins){
		$this->db->trans_begin();

		$this->db->where('int_periode_id', $int_periode_id);
		$this->db->update($this->m_periode, $ins);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function delete($int_periode_id){
		$this->db->trans_begin();
		$this->db->delete($this->m_periode,  ['int_periode_id' => $int_periode_id]);
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}
}