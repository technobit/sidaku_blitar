<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;

class Imp_pemutakhiran_model extends MY_Model {


	public function get_periode(){
		return $this->db->query("SELECT *
								 FROM	{$this->m_periode}
								 ORDER BY dt_periode DESC")->result();
	}

	public function get_list(){
		return $this->db->query("SELECT *
								 FROM	{$this->m_pemutakhiran_ktp}
								 ORDER BY var_nik ASC")->result();
	}

    public function list($periode_filter = "", $kategori_filter = "", $kecamatan_filter = 0, $kelurahan_filter = 0, $filter = NULL, $order_by = 0, $sort = 'ASC', $limit = 0, $ofset = 0){
		$this->db->select("*")
				->from($this->m_pemutakhiran_ktp." mpk")
				->join($this->m_kecamatan." mk", "mpk.int_kecamatan_id = mk.int_kecamatan_id", "left")
				->join($this->m_kelurahan." ml", "(mpk.`int_kecamatan_id` = ml.`int_kecamatan_id` AND mpk.`int_kelurahan_id` = ml.`int_kelurahan_id`)", "left");

		if($periode_filter != ""){ // filter
			$this->db->where('mpk.int_periode_id', $periode_filter);
		}

		if(($kategori_filter != "")){ // filter
			$this->db->where('mpk.int_kategori_ktp_id', $kategori_filter);
		}
			
		if(($kecamatan_filter != 0)){ // filter
			$this->db->where('mpk.int_kecamatan_id', $kecamatan_filter);
		}

		if(($kelurahan_filter != 0)){ // filter
			$this->db->where('mpk.int_kelurahan_id', $kelurahan_filter);
		}

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('mpk.var_nik', $filter)
					->or_like('mpk.var_nama', $filter)
					->or_like('mk.var_kecamatan', $filter)
					->or_like('ml.var_kelurahan', $filter)
					->group_end();
		}

		$order = 'var_nik ';
		switch($order_by){
			case 1 : $order = 'var_nik '; break;
			case 2 : $order = 'var_nama '; break;
			case 4 : $order = 'mk.var_kecamatan'; break;
			case 5 : $order = 'ml.var_kelurahan'; break;
		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($periode_filter = "", $kategori_filter = "", $kecamatan_filter = 0, $kelurahan_filter = 0,  $filter = NULL){
		$this->db->select("*")
				->from($this->m_pemutakhiran_ktp." mpk")
				->join($this->m_kecamatan." mk", "mpk.int_kecamatan_id = mk.int_kecamatan_id", "left")
				->join($this->m_kelurahan." ml", "(mpk.`int_kecamatan_id` = ml.`int_kecamatan_id` AND mpk.`int_kelurahan_id` = ml.`int_kelurahan_id`)", "left");

		if($periode_filter != ""){ // filter
			$this->db->where('mpk.int_periode_id', $periode_filter);
		}

		if(($kategori_filter != "")){ // filter
			$this->db->where('mpk.int_kategori_ktp_id', $kategori_filter);
		}
			
		if(($kecamatan_filter != 0)){ // filter
			$this->db->where('mpk.int_kecamatan_id', $kecamatan_filter);
		}

		if(($kelurahan_filter != 0)){ // filter
			$this->db->where('mpk.int_kelurahan_id', $kelurahan_filter);
		}

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
            $this->db->group_start()
			->like('mpk.var_nik', $filter)
			->or_like('mpk.var_nama', $filter)
			->or_like('mk.var_kecamatan', $filter)
			->or_like('ml.var_kelurahan', $filter)
			->group_end();
        }
		return $this->db->count_all_results();
	}

	public function get($int_mpemutakhiran_ktp_id){
		return $this->db->select("*")
					->get_where($this->m_pemutakhiran_ktp, ['int_mpemutakhiran_ktp_id' => $int_mpemutakhiran_ktp_id])->row();
	}

	public function update($int_mpemutakhiran_ktp_id, $ins){
		$this->db->trans_begin();

		$this->db->where('int_mpemutakhiran_ktp_id', $int_mpemutakhiran_ktp_id);
		$this->db->update($this->m_pemutakhiran_ktp, $ins);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function delete($int_mpemutakhiran_ktp_id){
		$this->db->trans_begin();
		$this->db->delete($this->m_pemutakhiran_ktp,  ['int_mpemutakhiran_ktp_id' => $int_mpemutakhiran_ktp_id]);
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function import($in, $file){
		$user 	= $this->session->userdata('username');
		$var_reg_no = 'A';
		$var_nik = 'B';
		$var_nama = 'C';
		$int_jeniskelamin_id = 'D';
		$var_tempat_lahir = 'E';
		$dt_tanggal_lahir = 'F';
		$int_agama_id = 'G';
		$int_status_kawin_id = 'H';
		$int_pendidikan_id = 'I';
		$int_pekerjaan_id = 'J';
		$var_alamat = 'K';
		$int_kecamatan_id = 'L';
		$int_kelurahan_id = 'M';
		$var_rw = 'N';
		$var_rt = 'O';

		$filterSubset = new MyReadFilter($in['mulai'],
						[$var_reg_no, $var_nik, $var_nama,
						$int_jeniskelamin_id,
						$var_tempat_lahir,
						$dt_tanggal_lahir,
						$int_agama_id,
						$int_status_kawin_id,
						$int_pendidikan_id,
						$int_pekerjaan_id,
						$var_alamat,
						$int_kecamatan_id,
						$int_kelurahan_id,
						$var_rw,
						$var_rt]
					);
		$reader = IOFactory::createReader(ucfirst(ltrim($file['file_ext'],'.')));
		$reader->setReadDataOnly(true);
		$reader->setReadFilter($filterSubset);
		$spreadsheet = $reader->load($file['full_path']);
		$data = $spreadsheet->getActiveSheet()->toArray(null, false, false, true);
		
		//INSERT IGNORE INTO-> 0 row affected if duplicate
		//REPLACE INTO -> replace old duplicate entry
		$ins_pemutakhiran_ktp = "REPLACE INTO {$this->m_pemutakhiran_ktp} 
						(`var_reg_no`,`var_nik`,`var_nama`,`int_jeniskelamin_id`,`var_tempat_lahir`,`dt_tanggal_lahir`,`int_agama_id`,`int_status_kawin_id`,
						`int_pendidikan_id`,`int_pekerjaan_id`,`var_alamat`,`int_kecamatan_id`,`int_kelurahan_id`,`var_rw`,`var_rt`,`int_periode_id`) VALUES ";
		$total = 0;
		foreach($data as $i => $d){
			if($i > ($in['mulai'] - 1)){
				$total++;
				

				$strtotime_tanggal_lahir = strtotime($d[$dt_tanggal_lahir]);
				$format_tanggal_lahir = date('Y-m-d',$strtotime_tanggal_lahir);
				
				$d_var_reg_no = $this->db->escape(trim($d[$var_reg_no]));
				$d_var_nik = $this->db->escape(trim($d[$var_nik]));
				$d_var_nama = $this->db->escape(trim($d[$var_nama]));
				$d_int_jeniskelamin_id = $this->db->escape(trim($d[$int_jeniskelamin_id]));
				$d_var_tempat_lahir = $this->db->escape(trim($d[$var_tempat_lahir]));
				$d_dt_tanggal_lahir = $this->db->escape(trim($format_tanggal_lahir));
				$d_int_agama_id = $this->db->escape(trim($d[$int_agama_id]));
				$d_int_status_kawin_id = $this->db->escape(trim($d[$int_status_kawin_id]));
				$d_int_pendidikan_id = $this->db->escape(trim($d[$int_pendidikan_id]));
				$d_int_pekerjaan_id = $this->db->escape(trim($d[$int_pekerjaan_id]));
				$d_var_alamat = $this->db->escape(trim($d[$var_alamat]));
				$d_int_kecamatan_id = $this->db->escape(trim($d[$int_kecamatan_id]));
				$d_int_kelurahan_id = $this->db->escape(trim($d[$int_kelurahan_id]));
				$d_var_rw = $this->db->escape($d[$var_rw]);
				$d_var_rt = $this->db->escape($d[$var_rt]);


				$ins_pemutakhiran_ktp .= "({$d_var_reg_no}, {$d_var_nik}, {$d_var_nama}, {$d_int_jeniskelamin_id}, {$d_var_tempat_lahir}, {$d_dt_tanggal_lahir}, {$d_int_agama_id}, {$d_int_status_kawin_id},
				{$d_int_pendidikan_id}, {$d_int_pekerjaan_id}, {$d_var_alamat}, {$d_int_kecamatan_id}, {$d_int_kelurahan_id}, {$d_var_rw}, {$d_var_rt}, {$in['int_periode_id']}),";
			}
		}
		$this->db->trans_begin();
		
		$this->db->insert($this->h_import, ['username' => $user,
										  'file_name' => $file['orig_name'],
										  'direktori' => $file['full_path'],
										  'total' => $total]);
		
		$ins_pemutakhiran_ktp = rtrim($ins_pemutakhiran_ktp, ',').';';
		$this->db->query($ins_pemutakhiran_ktp);
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return $total;
		}
	}
}

class MyReadFilter implements IReadFilter {
    private $startRow = 0;
    private $columns = [];

    public function __construct($startRow, $columns){
        $this->startRow = $startRow;
        $this->columns = $columns;
    }

    public function readCell($column, $row, $worksheetName = ''){
        if ($row >= $this->startRow) {
            if (in_array($column, $this->columns)) {
                return true;
            }
        }
        return false;
    }
}
