<div class="container-fluid">
    <div class="row">
        <section class="col-lg-12">
            <div class="card card-outline">
                <div class="card-header">
                    <h3 class="card-title mt-1">
                        <i class="<?=isset($breadcrumb->icon)? $breadcrumb->icon : 'far fa-circle'?>"></i>
                        <?=isset($breadcrumb->card_title)? $breadcrumb->card_title :  $breadcrumb->title?>
                    </h3>
                </div><!-- /.card-header -->
                <div class="card-body row" style="overflow:auto">
                    <div class="col-6">
                        <div class="form-group row mb-1">
                            <dd class="col-sm-4">ID. Keluarga</dd>
                            <dt class="col-sm-8" id="var_reg_no">
                                <?=isset($data->var_reg_no)? (empty($data->var_reg_no) ? '-' : $data->var_reg_no) : '-'?>
                                </dt>
                        </div>
                        <div class="form-group row mb-1">
                            <dd class="col-sm-4">NIK</dd>
                            <dt class="col-sm-8" id="var_nik"><?=isset($data->var_nik)? $data->var_nik : ''?></dt>
                        </div>
                        <div class="form-group row mb-1">
                            <dd class="col-sm-4">Nama</dd>
                            <dt class="col-sm-8" id="var_nama"><?=isset($data->var_nama)? $data->var_nama : ''?></dt>
                        </div>
                        <div class="form-group row mb-1">
                            <dd class="col-sm-4">Jenis Kelamin</dd>
                            <dt class="col-sm-8" id="var_jeniskelamin"><?=isset($data->var_jeniskelamin)? $data->var_jeniskelamin : ''?></dt>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group row mb-1">
                            <dd class="col-sm-4">Alamat</dd>
                            <dt class="col-sm-8" id="var_alamat"><?=isset($data->var_alamat)? $data->var_alamat : ''?></dt>
                        </div>
                        <div class="form-group row mb-1">
                            <dd class="col-sm-4">RT/RW</dd>
                            <dt class="col-sm-8" id="var_rt_rw">
                                <?=isset($data->var_rt)? $data->var_rt : ''?>/
                                <?=isset($data->var_rw)? $data->var_rw : ''?>
                            </dt>
                        </div>
                        <div class="form-group row mb-1">
                            <dd class="col-sm-4">Kelurahan</dd>
                            <dt class="col-sm-8" id="var_kelurahan"><?=isset($data->var_kelurahan)? $data->var_kelurahan : ''?></dt>
                        </div>
                        <div class="form-group row mb-1">
                            <dd class="col-sm-4">Kecamatan</dd>
                            <dt class="col-sm-8" id="var_kecamatan"><?=isset($data->var_kecamatan)? $data->var_kecamatan : ''?></dt>
                        </div>
                        <?php
                        if(isset($data->tpk_id)){
                            $mode = $data->tpk_id.'/get/';
                            $mode_color = 'warning';
                        }else{
                            $mode = $data->int_mpemutakhiran_ktp_id.'/add/';
                            $mode_color = 'primary';
                        }?>
                        <div class="form-group row mb-1">
                            <dd class="col-sm-4">No. Handphone</dd>
                            <dt class="col-sm-8" id="var_nohp"></dt>
                        </div>
                    </div>
                    <div class="col-12">
                        <table class="table table-striped table-hover table-full-width" id="datatable">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Data Sekarang</th>
                                <th>Data Pemutakhiran</th>
                                <th></th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<div id="ajax-modal" class="modal fade animate shake" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="75%" aria-hidden="true"></div>
<div id="ajax-modal-confirm" class="modal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="50%" aria-hidden="true"></div>
