<script src="<?=base_url()?>assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script src="<?=base_url() ?>assets/plugins/lightGallery/dist/js/lightgallery-all.min.js"></script>
<script src="<?=base_url() ?>assets/plugins/lightGallery/lib/jquery.mousewheel.min.js"></script>
<script>
    var dataTable;
    $(document).ready(function() {
		$('.select2').select2();
        dataTable = $('#datatable').DataTable({
            "bFilter": true,
            "bServerSide": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "paging":   false,
            "searching": false,
            "ordering": false,
            "info": false,
            "ajax": {
                "url": "<?=$url?>",
                "dataType": "json",
                "type": "POST",
                "data": function(d) {
                    d.<?=$page->tokenName ?> = $('meta[name=<?=$page->tokenName ?>]').attr("content");
                },
                "dataSrc": function(json) {
                    if (json.<?=$page->tokenName ?> !== undefined) $('meta[name=<?=$page->tokenName ?>]').attr("content", json.<?=$page->tokenName ?>);
                    $('#var_nohp').html(json.nohp);
                    return json.aaData;
                }
            },
            "aoColumns": [
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto",
                },
				{
                    "sWidth": "auto",
                    "bSearchable": false,
                    "bSortable": false
                }
            ]
        });
        $('.dataTables_filter input').unbind().bind('keyup', function(e) {
			if (e.keyCode == 13) {
				dataTable.search($(this).val()).draw();
			}
        });
        
    });
</script>