<form method="post" action="<?=$url ?>" role="form" class="form-horizontal" id="action" width="80%">
<?php // print_r($data);
	$stl_nohp = 'display:none';
	$stl_tempat_lahir = 'display:none';
	$stl_tanggal_lahir = 'display:none';
	$stl_agama = 'display:none';
	$stl_pendidikan = 'display:none';
	$stl_pekerjaan = 'display:none';
	$stl_perkawinan = 'display:none';
	switch ($mode) {
		case 0:
			$stl_nohp = 'display:block';
			$var_jenis_img = 'handphone';
			break;
		case 1:
			$stl_tempat_lahir = 'display:block';
			$var_jenis_img = 'tempat_lahir';
			break;
		case 2:
			$stl_tanggal_lahir = 'display:block';
			$var_jenis_img = 'tanggal_lahir';
			break;
		case 3:
			$stl_agama = 'display:block';
			$var_jenis_img = 'agama';
			break;
		case 4:
			$stl_pendidikan = 'display:block';
			$var_jenis_img = 'pendidikan';
			break;
		case 5:
			$stl_pekerjaan = 'display:block';
			$var_jenis_img = 'pekerjaan';
			break;
		case 6:
			$stl_perkawinan = 'display:block';
			$var_jenis_img = 'status_kawin';
			break;
	}
?>
<div id="modal-form" class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
		<div class="modal-header pb-0">
			<ul class="nav nav-tabs no-border-b" id="custom-content-below-tab" role="tablist" style="width:100%">
				<li class="nav-item">
					<a class="nav-link active" id="index-form-input-tab" data-toggle="pill" href="#index-form-input" role="tab" aria-controls="index-form-input" aria-selected="true"><?=$title?></a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="index-form-lampiran-tab" data-toggle="pill" href="#index-form-lampiran" role="tab" aria-controls="index-form-lampiran" aria-selected="false">Lampiran</a>
				</li>
			</ul>	
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="tab-content pt-1" id="custom-content-below-tabContent">
				<div class="form-message text-center"></div>
				<div class="tab-pane fade show active" id="index-form-input" role="tabpanel" aria-labelledby="index-form-input-tab">
					<div class="card">
						<div class="card-body">
							<div id="fld_tempat_lahir" style="<?=$stl_tempat_lahir?>">
								<div class="form-group row mb-1">
									<label for="var_tempat_lahir" class="col-sm-4 col-form-label">Tempat Lahir</label>
									<div class="col-sm-8" >
										<select id="var_tempat_lahir" name="var_tempat_lahir" class="form-control form-control-sm select2" style="width: 100%;">
											<option value="0">- Pilih Tempat Lahir -</option>
											<?php 
												foreach($kota_kab as $kk){
													echo '<option value="'.$kk->var_kota_kab.'">'.$kk->var_kota_kab.'</option>';
												}
											?>
										</select>
									</div>
								</div>
								<div class="form-group row mb-1">
									<label for="var_ket_tempat_lahir" class="col-sm-4 col-form-label">Keterangan</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_ket_tempat_lahir" placeholder="Keterangan" name="var_ket_tempat_lahir" value="<?=isset($data->var_ket_tempat_lahir)? $data->var_ket_tempat_lahir : ''?>" />
									</div>
								</div>
							</div>
							<div id="fld_tanggal_lahir" style="<?=$stl_tanggal_lahir?>">
								<div class="form-group row mb-1">
									<label for="dt_tanggal_lahir" class="col-sm-4 col-form-label">Tanggal Lahir</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm date_picker" id="dt_tanggal_lahir" placeholder="Tanggal Lahir" name="dt_tanggal_lahir" value="<?=isset($data->dt_tanggal_lahir)? $data->dt_tanggal_lahir : '0'?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label for="var_ket_tanggal_lahir" class="col-sm-4 col-form-label">Keterangan</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_ket_tanggal_lahir" placeholder="Keterangan" name="var_ket_tanggal_lahir" value="<?=isset($data->var_ket_tanggal_lahir)? $data->var_ket_tanggal_lahir : ''?>" />
									</div>
								</div>
							</div>
							<div id="fld_agama" style="<?=$stl_agama?>">
								<div class="form-group row mb-1">
									<label for="int_agama_id" class="col-sm-4 col-form-label">Agama</label>
									<div class="col-sm-8" >
										<select id="int_agama_id" name="int_agama_id" class="form-control form-control-sm select2" style="width: 100%;">
											<option value="0">- Pilih Agama -</option>
											<?php 
												foreach($agama as $ag){
													echo '<option value="'.$ag->int_agama_id.'">'.$ag->var_agama.'</option>';
												}
											?>
										</select>
									</div>
								</div>
								<div class="form-group row mb-1">
									<label for="var_ket_agama" class="col-sm-4 col-form-label">Keterangan</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_ket_agama" placeholder="Keterangan" name="var_ket_agama" value="<?=isset($data->var_ket_agama)? $data->var_ket_agama : ''?>" />
									</div>
								</div>
							</div>
							<div id="fld_pendidikan" style="<?=$stl_pendidikan?>">
								<div class="form-group row mb-1">
									<label for="int_pendidikan_id" class="col-sm-4 col-form-label">Pendidikan</label>
									<div class="col-sm-8" >
										<select id="int_pendidikan_id" name="int_pendidikan_id" class="form-control form-control-sm select2" style="width: 100%;">
											<option value="0">- Pilih Pendidikan -</option>
											<?php 
												foreach($pendidikan as $pn){
													echo '<option value="'.$pn->int_pendidikan_id.'">'.$pn->var_pendidikan.'</option>';
												}
											?>
										</select>
									</div>
								</div>
								<div class="form-group row mb-1">
									<label for="var_ket_pendidikan" class="col-sm-4 col-form-label">Keterangan</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_ket_pendidikan" placeholder="Keterangan" name="var_ket_pendidikan" value="<?=isset($data->var_ket_pendidikan)? $data->var_ket_pendidikan : ''?>" />
									</div>
								</div>
							</div>
							<div id="fld_pekerjaan" style="<?=$stl_pekerjaan?>">
								<div class="form-group row mb-1">
									<label for="int_pekerjaan_id" class="col-sm-4 col-form-label">Keterangan</label>
									<div class="col-sm-8" >
										<select id="int_pekerjaan_id" name="int_pekerjaan_id" class="form-control form-control-sm select2" style="width: 100%;">
											<option value="0">- Pilih Pekerjaan -</option>
											<?php 
												foreach($pekerjaan as $pk){
													echo '<option value="'.$pk->int_pekerjaan_id.'">'.$pk->var_pekerjaan.'</option>';
												}
											?>
										</select>
									</div>
								</div>
								<div class="form-group row mb-1">
									<label for="var_ket_pekerjaan" class="col-sm-4 col-form-label">Keterangan</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_ket_pekerjaan" placeholder="Keterangan" name="var_ket_pekerjaan" value="<?=isset($data->var_ket_pekerjaan)? $data->var_ket_pekerjaan : ''?>" />
									</div>
								</div>
							</div>
							<div id="fld_perkawinan" style="<?=$stl_perkawinan?>">
								<div class="form-group row mb-1">
									<label for="int_status_kawin_id" class="col-sm-4 col-form-label">Status Perkawinan</label>
									<div class="col-sm-8" >
										<select id="int_status_kawin_id" name="int_status_kawin_id" class="form-control form-control-sm select2" style="width: 100%;">
											<option value="0">- Pilih Status Perkawinan -</option>
											<?php 
												foreach($status_kawin as $sk){
													echo '<option value="'.$sk->int_status_kawin_id.'">'.$sk->var_status_kawin.'</option>';
												}
											?>
										</select>
									</div>
								</div>
								<div class="form-group row mb-1">
									<label for="var_ket_status_kawin" class="col-sm-4 col-form-label">Keterangan</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_ket_status_kawin" placeholder="Keterangan" name="var_ket_status_kawin" value="<?=isset($data->var_ket_status_kawin)? $data->var_ket_status_kawin : ''?>" />
									</div>
								</div>
							</div>
							<div id="fld_nohp" style="<?=$stl_nohp?>">
								<div class="form-group row mb-1">
									<label for="var_nohp" class="col-sm-4 col-form-label">No. Handphone</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_nohp" placeholder="Nomor HP" name="var_nohp" value="<?=isset($data->var_nohp)? $data->var_nohp : ''?>" />
									</div>
								</div>
							</div>
							<input type="hidden" id="var_reg_no" name="var_reg_no" value="<?=isset($data->var_reg_no)? $data->var_reg_no : ''?>"/>
							<input type="hidden" id="var_nik" name="var_nik" value="<?=isset($data->var_nik)? $data->var_nik : ''?>"/>
							<input type="hidden" id="var_nama" name="var_nama" value="<?=isset($data->var_nama)? $data->var_nama : ''?>"/>
							<input type="hidden" id="int_periode_id" name="int_periode_id" value="<?=isset($data->int_periode_id)? $data->int_periode_id : ''?>"/>
							<input type="hidden" id="var_jenis_img" name="var_jenis_img" value="<?=isset($data->var_jenis_img)? $data->var_jenis_img : $var_jenis_img?>"/>
						</div>
					</div>
				</div>
				<div class="tab-pane fade" id="index-form-lampiran" role="tabpanel" aria-labelledby="index-form-lampiran-tab">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group row mb-1">
								<div class="col-md-12">
									<div class="custom-file">
										<input type="file" class="custom-file-input" name="<?=$input_file_name?>[]" id="customFile" accept="image/*" multiple>
										<label class="custom-file-label" for="customFile">Choose file</label>
										<span class="required"><i>*Ukuran Gambar Maksimal 2MB</i?<span>
									</div>
								</div>
							</div>
							<div class="row preview-images-zone">
							<?php 
								if(isset($data->lampiran)):
									foreach($data->lampiran as $lmp):
										if($lmp->var_jenis_img == $var_jenis_img):
							?>
								<div class=" col-sm-4 img-server img-<?=$lmp->int_pemutakhiran_img_id?>">
									<div class="preview-image preview-show-<?=$lmp->int_pemutakhiran_img_id?> img-popup">
										<div class="image-zone" data-responsive="<?=cdn_url().$lmp->var_image ?> 375, <?=cdn_url().$lmp->var_image ?> 480, <?=cdn_url().$lmp->var_image ?> 800" data-src="<?=cdn_url().$lmp->var_image ?>" data-sub-html="<?=$lmp->var_jenis_img?>">
											<a href="<?=cdn_url().$lmp->var_image ?>">
												<img id="pro-img-<?=$lmp->int_pemutakhiran_img_id?>" class="img-thumb-sm" src="<?=cdn_url().$lmp->var_image ?>">
											</a>
										</div>
									</div>
									<a href="#" class="btn btn-sm btn-danger ajax_modal_confirm" data-url="<?=site_url('ver_brm/'.$lmp->int_pemutakhiran_img_id.'/lampiran')?>" data-block="#modal-form" style="display: block;width: 260px;margin-top: 0;margin-bottom: 10px;margin-left: auto;margin-right: auto;"><i class="fa fa-trash"></i> </a> 
								</div>
							
							<?php 
										endif;
									endforeach;
								endif;
							?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-danger">Keluar</button>
			<button type="submit" class="btn btn-success" id="save_button">Simpan</button>
		</div>
	</div>
</div>
<?=form_close() ?>

<script>
	$(document).ready(function(){
		$('.select2').select2();
		$(".img-popup").lightGallery();
		$('.date_picker').daterangepicker(datepickModal);

		<?php if(isset($data->var_tempat_lahir)) echo '$("#var_tempat_lahir").val("'.$data->var_tempat_lahir.'").trigger("change");'?>
		<?php if(isset($data->int_agama_id)) echo '$("#int_agama_id").val("'.$data->int_agama_id.'").trigger("change");'?>
		<?php if(isset($data->int_pendidikan_id)) echo '$("#int_pendidikan_id").val("'.$data->int_pendidikan_id.'").trigger("change");'?>
		<?php if(isset($data->int_pekerjaan_id)) echo '$("#int_pekerjaan_id").val("'.$data->int_pekerjaan_id.'").trigger("change");'?>
		<?php if(isset($data->int_status_kawin_id)) echo '$("#int_status_kawin_id").val("'.$data->int_status_kawin_id.'").trigger("change");'?>

		$("#action").validate({
			rules: {
			    var_nik:{
					required: true,
					digits: true,
					minlength: 16,
					maxlength: 16
				},
			    var_nohp:{
			        required: true,
					digits: true,
					minlength: 10,
					maxlength: 13
				}
			},
			submitHandler: function(form) {
				$('.form-message').html('');
                let blc = '#modal-form';
                blockUI(blc);
				$(form).ajaxSubmit({
					dataType:  'json',
					data: {<?=$page->tokenName ?> : $('meta[name=<?=$page->tokenName ?>]').attr("content")},
					success: function(data){
                        unblockUI(blc);
                        setFormMessage('.form-message', data);
                        if(data.stat){
							resetForm('#action');
							dataTable.draw();
						}
						closeModal($modal, data);
						if(data.hasOwnProperty('files')){
							$.each(data.files, function(i, v){
								if(v.stat){
									if(!$('.'+v.cdImg).hasClass('is-valid')) $('.title-image-'+i).addClass('is-valid');
									if(!$('.'+v.cdImg).next('div').hasClass('valid-feedback'))$('.title-image-'+i).after('<div id="'+i+'-valid" class="valid-feedback">'+v.msg+'</div>');
								}else{
									if(!$('.'+v.cdImg).hasClass('is-invalid')) $('.'+v.cdImg).addClass('is-invalid');
									if(!$('.'+v.cdImg).next('div').hasClass('invalid-feedback'))$('.'+v.cdImg).after('<div id="'+i+'-error" class="invalid-feedback">'+v.msg+'</div>');
								}
								
								
							});
						}
					}
				});
			},
			validClass: "valid-feedback",
			errorElement: "div", // contain the error msg in a small tag
			errorClass: 'invalid-feedback',
			errorPlacement: erp,
			highlight: hl,
			unhighlight: uhl,
			success: sc
		});

		bsCustomFileInput.init();
		$( ".preview-images-zone" ).sortable();
		$(document).on('click', '.image-cancel', function() {
			let no = $(this).data('no');
			//$(".preview-image.preview-show-"+no).parent().parent().remove();
			$(this).parent().parent().remove();
		});
		
		var num = 0;
		$('#customFile').change(function(){
			if (window.File && window.FileList && window.FileReader) {
			$('.preview-images-zone').find('div.img-client').remove();
			var files = event.target.files; //FileList object
			for (let i = 0; i < files.length; i++) {
				var file = files[i];
				if (!file.type.match('image')) continue;
				
				var picReader = new FileReader();
				let cdImg 	= 'img'+getSandStr(3,'-',3);
				let imgSize = (file.size/1000).toFixed(2);
				let imgType = file.type;
				
				picReader.addEventListener('load', function (event) {
					var picFile = event.target;
					var html =  '<div class="col-sm-4 img-client"><input type="hidden" name="cdImg['+i+']" value="'+cdImg+'"/>' +
								'<div class="preview-image preview-show-' + i + '">' +
								'<div class="image-zone"><img id="pro-img-' + i + '" src="' + picFile.result + '"></div>' +
								'</div><input type="hidden" class="form-control form-control-sm '+cdImg+'" disabled/></div>';
					$(html).appendTo(".preview-images-zone");
				});

				//$( ".preview-images-zone" ).sortable();
				picReader.readAsDataURL(file);
			}
				$("#pro-image").val('');
			}else {
				alert('Browser not support');
			}
		});
	});
</script>