<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['ver_brm']['get']                                = 'verifikasi/brm';
$route['ver_brm']['post']                               = 'verifikasi/brm/list';
$route['ver_brm/([0-9]+)/add']['get']                   = 'verifikasi/brm/add/$1';
$route['ver_brm/save']['post']                          = 'verifikasi/brm/save';
$route['ver_brm/([a-zA-Z0-9]+)/([a-zA-Z0-9]+)']['get']  = 'verifikasi/brm/get/$1/$2';
$route['ver_brm/([a-zA-Z0-9]+)']['post']                = 'verifikasi/brm/update/$1';
$route['ver_brm/([a-zA-Z0-9]+)/([a-zA-Z0-9]+)/del']['get'] = 'verifikasi/brm/confirm/$1/$2';
$route['ver_brm/([a-zA-Z0-9]+)/del']['post']            = 'verifikasi/brm/delete/$1';
$route['ver_brm/([0-9]+)/lampiran']['get']              = 'verifikasi/brm/confirm_del_lampiran/$1';
$route['ver_brm/([0-9]+)/lampiran_del']['post']         = 'verifikasi/brm/delete_lampiran/$1';

$route['ver_bck']['get']                                = 'verifikasi/bck';
$route['ver_bck']['post']                               = 'verifikasi/bck/list';
$route['ver_bck/([0-9]+)/add']['get']                   = 'verifikasi/bck/add/$1';
$route['ver_bck/save']['post']                          = 'verifikasi/bck/save';
$route['ver_bck/([a-zA-Z0-9]+)/([a-zA-Z0-9]+)']['get']  = 'verifikasi/bck/get/$1/$2';
$route['ver_bck/([a-zA-Z0-9]+)']['post']                = 'verifikasi/bck/update/$1';
$route['ver_bck/([a-zA-Z0-9]+)/([a-zA-Z0-9]+)/del']['get']  = 'verifikasi/bck/confirm/$1$2';
$route['ver_bck/([a-zA-Z0-9]+)/del']['post']            = 'verifikasi/bck/delete/$1';
$route['ver_bck/([0-9]+)/lampiran']['get']              = 'verifikasi/bck/confirm_del_lampiran/$1';
$route['ver_bck/([0-9]+)/lampiran_del']['post']         = 'verifikasi/bck/delete_lampiran/$1';

$route['ver_kba']['get']                                = 'verifikasi/kba';
$route['ver_kba']['post']                               = 'verifikasi/kba/list';
$route['ver_kba/([0-9]+)/add']['get']                   = 'verifikasi/kba/add/$1';
$route['ver_kba/save']['post']                          = 'verifikasi/kba/save';
$route['ver_kba/([a-zA-Z0-9]+)/([a-zA-Z0-9]+)']['get']  = 'verifikasi/kba/get/$1/$2';
$route['ver_kba/([a-zA-Z0-9]+)']['post']                = 'verifikasi/kba/update/$1';
$route['ver_kba/([a-zA-Z0-9]+)/([a-zA-Z0-9]+)/del']['get']  = 'verifikasi/kba/confirm/$1$2';
$route['ver_kba/([a-zA-Z0-9]+)/del']['post']            = 'verifikasi/kba/delete/$1';
$route['ver_kba/([0-9]+)/lampiran']['get']              = 'verifikasi/kba/confirm_del_lampiran/$1';
$route['ver_kba/([0-9]+)/lampiran_del']['post']         = 'verifikasi/kba/delete_lampiran/$1';

$route['pemutakhiran']['get']                       = 'verifikasi/pemutakhiran';
$route['pemutakhiran']['post']                      = 'verifikasi/pemutakhiran/list';
$route['pemutakhiran/([a-zA-Z0-9]+)']['get']        = 'verifikasi/pemutakhiran/detail/$1';
$route['pemutakhiran/([a-zA-Z0-9]+)']['post']       = 'verifikasi/pemutakhiran/detail_list/$1';
$route['pemutakhiran/([0-9]+)/add/([0-9]+)']['get'] = 'verifikasi/pemutakhiran/add/$1/$2';
$route['pemutakhiran/([0-9]+)/add/([0-9]+)']['post']= 'verifikasi/pemutakhiran/create/$1/$2';
$route['pemutakhiran/([0-9]+)/get/([0-9]+)']['get']= 'verifikasi/pemutakhiran/get/$1/$2';
$route['pemutakhiran/([0-9]+)/upd/([0-9]+)']['post']= 'verifikasi/pemutakhiran/update/$1/$2';

$route['pemutakhiran/([a-zA-Z0-9]+)/del']['get']     = 'verifikasi/pemutakhiran/confirm/$1';
$route['pemutakhiran/([a-zA-Z0-9]+)/del']['post']    = 'verifikasi/pemutakhiran/delete/$1';
$route['pemutakhiran/([0-9]+)/lampiran']['get']      = 'verifikasi/pemutakhiran/confirm_del_lampiran/$1';
$route['pemutakhiran/([0-9]+)/lampiran_del']['post'] = 'verifikasi/pemutakhiran/delete_lampiran/$1';
