<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
class Pemutakhiran extends MX_Controller {
	private $input_file_name = 'lampiran';

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'PEMUTAKHIRAN'; // kode data pada tabel data, 1 data : 1 controller
		$this->module   = 'verifikasi';
		$this->routeURL = 'pemutakhiran';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('pemutakhiran_model', 'model');
		$this->load->model('master/wilayah_model', 'wilayah');
		$this->load->model('master/periode_model', 'periode');

    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Pemutakhiran Elemen Data KTP';
		$this->page->menu 	  = 'pemutakhiran';
		$this->breadcrumb->title = 'Pemutakhiran Elemen Data KTP';
		$this->breadcrumb->card_title = 'Pemutakhiran Elemen Data KTP';
		$this->breadcrumb->icon = 'fas fa-camera-retro';
		$this->breadcrumb->list = ['Pemutakhiran Elemen Data KTP'];
		$this->css = true;
		$this->js = true;
		$data['periode']		= $this->periode->get_periode();
		$data['kecamatan_usr']	= $this->session->userdata['int_kecamatan_id'];
		$data['kelurahan_usr']	= $this->session->userdata['int_kelurahan_id'];
		$data['list_kecamatan'] = $this->wilayah->get_kecamatan_usr();
		$data['url'] = site_url("{$this->routeURL}/add");
		$this->render_view('pemutakhiran/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r'); 

		$data  = array();
		$total = $this->model->listCount($this->input->post('periode_filter', true), $this->input->post('kecamatan_filter', true), $this->input->post('kelurahan_filter', true), $this->input->post('status_filter', true), $this->input_post('search[value]', TRUE));
		$ldata = $this->model->list($this->input->post('periode_filter', true), $this->input->post('kecamatan_filter', true), $this->input->post('kelurahan_filter', true), $this->input->post('status_filter', true), $this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

		$i 	   = $this->input_post('start', true);
		foreach($ldata as $d){
			$i++;
			$action = '<div style="width:80px;display:inline-block">';
			if($d->int_pemutakhiran_ktp_id){
				$action .= '<a href="'.site_url("{$this->routeURL}/").$d->int_mpemutakhiran_ktp_id.'" class="btn btn-xs btn-warning tooltips" ><i class="fa fa-edit"></i> Ubah</a> ';
			}else{
				$action .= '<a href="'.site_url("{$this->routeURL}/").$d->int_mpemutakhiran_ktp_id.'" class="btn btn-xs btn-primary tooltips" ><i class="fa fa-edit"></i> Tambah</a> ';
			}
			$action .= '</div>';
			
			$data[] = array($i.'.', $d->var_reg_no, $d->var_nik, $d->var_nama, $d->var_tempat_lahir.', '.(idn_date($d->dt_tanggal_lahir, "j F Y")), $d->var_kecamatan, $d->var_kelurahan, isset($d->created_at) ? idn_date($d->created_at, "j F Y") :  "-", $action);
		}
		$this->set_json(array( 'stat' => TRUE,
								'iTotalRecords' => $total,
								'iTotalDisplayRecords' => $total,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()),200,false);
	}

	public function detail($int_mpemutakhiran_ktp_id){
		if($this->authCheckDetailAccess('u', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->detail($int_mpemutakhiran_ktp_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Error', 'title' => 'Data Not Found', 'message' => '']],true);
		}else{
			$this->page->subtitle = 'Pemutakhiran Elemen Data KTP';
			$this->page->menu 	  = 'pemutakhiran';
			$this->breadcrumb->title = 'Pemutakhiran Elemen Data KTP';
			$this->breadcrumb->card_title = 'Pemutakhiran Elemen Data KTP';
			$this->breadcrumb->icon = 'fas fa-camera-retro';
			$this->breadcrumb->list = ['Pemutakhiran Elemen Data KTP'];
			$this->js = true;

			$data['data'] 	= $res;
			$data['url']	= site_url("{$this->routeURL}/$int_mpemutakhiran_ktp_id");
			$data['title']	= 'Edit BRM';
			$data['input_file_name'] = $this->input_file_name;
			$this->render_view('pemutakhiran/index_detail', $data);
		}
		
	}

	public function detail_list($int_mpemutakhiran_ktp_id){
		$this->authCheckDetailAccess('r'); 

		$data  = array();
		$detail = $this->model->detail_list($int_mpemutakhiran_ktp_id);

		if(isset($detail->int_pemutakhiran_ktp_id)){
			$mode = $detail->int_pemutakhiran_ktp_id.'/get/';
			$mode_color = 'warning';
		}else{
			$mode = $int_mpemutakhiran_ktp_id.'/add/';
			$mode_color = 'primary';
		}
			
		$data[] = array('<b>Tempat Lahir</b>', $detail->var_tempat_lahir, 
						isset($detail->new_var_tempat_lahir) ? ($detail->new_var_tempat_lahir == '0' ? 'nol' :  $detail->new_var_tempat_lahir) : '-',
						'<a href="#" data-block="body" data-url="'.site_url("{$this->routeURL}/").$mode.'1" class="ajax_modal btn btn-xs btn-'.$mode_color.' tooltips" data-placement="top" data-original-title="Edit" ><i class="fa fa-edit"></i> Ubah</a>');
		$data[] = array('<b>Tanggal Lahir</b>', idn_date($detail->dt_tanggal_lahir, "j F Y"), 
						isset($detail->new_dt_tanggal_lahir)? ($detail->new_dt_tanggal_lahir == '0000-00-00'? '-' : idn_date($detail->new_dt_tanggal_lahir, "j F Y")) : '-', 
						'<a href="#" data-block="body" data-url="'.site_url("{$this->routeURL}/").$mode.'2" class="ajax_modal btn btn-xs btn-'.$mode_color.' tooltips" data-placement="top" data-original-title="Edit" ><i class="fa fa-edit"></i> Ubah</a>');
		$data[] = array('<b>Agama</b>', $detail->var_agama, 
						isset($detail->new_var_agama)? $detail->new_var_agama : '-', 
						'<a href="#" data-block="body" data-url="'.site_url("{$this->routeURL}/").$mode.'3" class="ajax_modal btn btn-xs btn-'.$mode_color.' tooltips" data-placement="top" data-original-title="Edit" ><i class="fa fa-edit"></i> Ubah</a>');
		$data[] = array('<b>Pendidikan</b>', $detail->var_pendidikan, 
						isset($detail->new_var_pendidikan)? $detail->new_var_pendidikan : '-', 
						'<a href="#" data-block="body" data-url="'.site_url("{$this->routeURL}/").$mode.'4" class="ajax_modal btn btn-xs btn-'.$mode_color.' tooltips" data-placement="top" data-original-title="Edit" ><i class="fa fa-edit"></i> Ubah</a>');
		$data[] = array('<b>Pekerjaan</b>', $detail->var_pekerjaan, 
						isset($detail->new_var_pekerjaan)? $detail->new_var_pekerjaan : '-', 
						'<a href="#" data-block="body" data-url="'.site_url("{$this->routeURL}/").$mode.'5" class="ajax_modal btn btn-xs btn-'.$mode_color.' tooltips" data-placement="top" data-original-title="Edit" ><i class="fa fa-edit"></i> Ubah</a>');
		$data[] = array('<b>Status Perkawinan</b>', $detail->var_status_kawin, 
						isset($detail->new_var_status_kawin)? $detail->new_var_status_kawin : '-', 
						'<a href="#" data-block="body" data-url="'.site_url("{$this->routeURL}/").$mode.'6" class="ajax_modal btn btn-xs btn-'.$mode_color.' tooltips" data-placement="top" data-original-title="Edit" ><i class="fa fa-edit"></i> Ubah</a>');
		if(isset($detail->int_pemutakhiran_ktp_id)){
			$data[] = array('', '', 
			'<a href="#" data-block="body" data-url="'.site_url("{$this->routeURL}/").$detail->int_pemutakhiran_ktp_id.'/del" class="ajax_modal btn btn-sm btn-danger" tooltips" data-placement="top" data-original-title="Delete" ><i class="fa fa-trash"> </i> Hapus Pemutakhiran</a>', '');

		}
		$var_nohp = isset($detail->var_nohp)? $detail->var_nohp : '';
		$form_hp =  $var_nohp.' <a href="#" data-block="body" data-url="'.site_url("{$this->routeURL}/").$mode.'0" class="ajax_modal btn btn-xs btn-'.$mode_color.' tooltips" data-placement="top" data-original-title="Edit" ><i class="fa fa-edit"></i> Ubah</a>';

		$this->set_json(array( 'stat' => TRUE,
								'nohp' => $form_hp,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()),200,false);
	}

	public function add($int_mpemutakhiran_ktp_id, $mode){
		if($this->authCheckDetailAccess('c', true) == false) return; // hak akses untuk modal popup

		$data['url']			= site_url("{$this->routeURL}/{$int_mpemutakhiran_ktp_id}/add/{$mode}");
		$data['title']			= 'Formulir Pemuktahiran Elemen Data KTP';
		$data['data']			= $this->model->add_form($int_mpemutakhiran_ktp_id);
		$data['mode']			= $mode;
		$data['agama']			= $this->model->get_agama();
		$data['pendidikan']		= $this->model->get_pendidikan();
		$data['pekerjaan']		= $this->model->get_pekerjaan();
		$data['status_kawin']	= $this->model->get_status_kawin();
		$data['kota_kab']		= $this->model->get_kota_kab();
		$data['input_file_name']= $this->input_file_name;
		$this->load_view('pemutakhiran/index_action', $data, true);
	}

	public function create($int_mpemutakhiran_ktp_id, $mode){
		$this->authCheckDetailAccess('c');

		$year = date("Y");
		$month = date("m");
		$img_dir = $this->config->item('img_dir').$year."/".$month; 
		$upload_path = $this->config->item('upload_path').$year."/".$month; 
		if (!file_exists($upload_path)) {
			mkdir($upload_path, 0777, true);
		}

		$this->form_validation->set_rules('var_nik', 'NIK', 'required|numeric|min_length[16]|max_length[16]');

		$post_data = $this->input->post();
		if($post_data['dt_tanggal_lahir'] == '0000-01-01'){
			$post_data['dt_tanggal_lahir'] = '0000-00-00';
		}
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'mc' => false, //modal close
								'msg' => "Data Validation Failed",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        } else {
			$lampiran_insert  = [];
			$lampiran_msg 	= [];
			$countFiles		= 0;
			$files 			= [];
			$img_up_stat	= true;
			if(isset($_FILES[$this->input_file_name])){		
				$config['upload_path']   = $upload_path;
				$config['allowed_types'] = $this->config->item('allowed_types'); 
				$config['encrypt_name']  = $this->config->item('encrypt_name');
				$config['max_size']      = $this->config->item('max_size');

				$this->load->library('upload', $config);
				
				$countFiles = count($_FILES[$this->input_file_name]['name']);
				$cdImg		= $this->input->post('cdImg');
				
				for ($i = 0; $i < $countFiles ; $i++) { 
					if(!empty($_FILES[$this->input_file_name]['name'][$i])){
						$_FILES['gambar']['name']     = $_FILES[$this->input_file_name]['name'][$i];
						$_FILES['gambar']['type']     = $_FILES[$this->input_file_name]['type'][$i];
						$_FILES['gambar']['tmp_name'] = $_FILES[$this->input_file_name]['tmp_name'][$i];
						$_FILES['gambar']['error']    = $_FILES[$this->input_file_name]['error'][$i];
						$_FILES['gambar']['size']     = $_FILES[$this->input_file_name]['size'][$i];
						
						if($this->upload->do_upload('gambar')){
							$lampiran_insert[] 	= array_merge($this->upload->data(), 
																['dir' => $img_dir]);
							$lampiran_msg[$i] = ['stat' => true, 'msg' => 'Upload Success', 'cdImg' => $cdImg[$i]];
						}else{
							$lampiran_msg[$i] = ['stat' => false, 'msg' => $this->upload->display_errors('',''), 'cdImg' => $cdImg[$i]];
							$img_up_stat = false;
						}
						$this->upload->error_msg = [];
					}
				}
				$files = ['files' => $lampiran_msg];
			}
			
			if($img_up_stat==true){
				$check = $this->model->create($post_data, $lampiran_insert);
				if($check==true){
					$json_status = true;
					$json_mc = true;
					$json_msg = "Data Berhasil Disimpan";	
				}else{
					$json_status = false;
					$json_mc = false;
					$json_msg = "Data Gagal Disimpan";
				}
			}else{
					$json_status = false;
					$json_mc = false;
					$json_msg = "Data Gagal Disimpan";
			}

			$this->set_json(array_merge([ 'stat' => $json_status, 
								'mc' => $json_mc,
								'msg' => $json_msg,
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							], $files));
        }
	}
	
	public function get($int_pemutakhiran_ktp_id, $mode){
		if($this->authCheckDetailAccess('u', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get_edit($int_pemutakhiran_ktp_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Error', 'title' => 'Data Not Found', 'message' => '']],true);
		}else{
			$data['data']			= $res;
			$data['url']			= site_url("{$this->routeURL}/{$int_pemutakhiran_ktp_id}/upd/{$mode}");
			$data['title']			= 'Edit BRM';
			$data['mode']			= $mode;
			$data['agama']			= $this->model->get_agama();
			$data['pendidikan']		= $this->model->get_pendidikan();
			$data['pekerjaan']		= $this->model->get_pekerjaan();
			$data['status_kawin']	= $this->model->get_status_kawin();
			$data['kota_kab']		= $this->model->get_kota_kab();
			$data['input_file_name']= $this->input_file_name;
			$this->load_view('pemutakhiran/index_action', $data);
		}
		
	}

	public function update($int_pemutakhiran_ktp_id){
		$this->authCheckDetailAccess('u');

		$year = date("Y");
		$month = date("m");
		$img_dir = $this->config->item('img_dir').$year."/".$month; 
		$upload_path = $this->config->item('upload_path').$year."/".$month; 
		if (!file_exists($upload_path)) {
			mkdir($upload_path, 0777, true);
		}
		$this->form_validation->set_rules('var_nik', 'NIK', 'required|numeric|min_length[16]|max_length[16]');

		$post_data = $this->input->post();
		if($post_data['dt_tanggal_lahir'] == '0000-01-01'){
			$post_data['dt_tanggal_lahir'] = '0000-00-00';
		}
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'msg' => "Data Validation Failed",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        }  else {
			$lampiran_insert  = [];
			$lampiran_msg 	= [];
			$countFiles		= 0;
			$files 			= [];
			$img_up_stat	= true;
			if(isset($_FILES[$this->input_file_name])){		
				$config['upload_path']   = $upload_path;
				$config['allowed_types'] = $this->config->item('allowed_types'); 
				$config['encrypt_name']  = $this->config->item('encrypt_name');
				$config['max_size']      = $this->config->item('max_size');

				$this->load->library('upload', $config);
				
				$countFiles = count($_FILES[$this->input_file_name]['name']);
				$cdImg		= $this->input->post('cdImg');
				
				for ($i = 0; $i < $countFiles ; $i++) { 
					if(!empty($_FILES[$this->input_file_name]['name'][$i])){
						$_FILES['gambar']['name']     = $_FILES[$this->input_file_name]['name'][$i];
						$_FILES['gambar']['type']     = $_FILES[$this->input_file_name]['type'][$i];
						$_FILES['gambar']['tmp_name'] = $_FILES[$this->input_file_name]['tmp_name'][$i];
						$_FILES['gambar']['error']    = $_FILES[$this->input_file_name]['error'][$i];
						$_FILES['gambar']['size']     = $_FILES[$this->input_file_name]['size'][$i];
						
						if($this->upload->do_upload('gambar')){
							$lampiran_insert[] 	= array_merge($this->upload->data(), 
																['dir' => $img_dir]);
							$lampiran_msg[$i] = ['stat' => true, 'msg' => 'Upload Success', 'cdImg' => $cdImg[$i]];
						}else{
							$lampiran_msg[$i] = ['stat' => false, 'msg' => $this->upload->display_errors('',''), 'cdImg' => $cdImg[$i]];
							$img_up_stat = false;
						}
						$this->upload->error_msg = [];
					}
				}
				$files = ['files' => $lampiran_msg];
				if($img_up_stat==true){
					$check = $this->model->update($int_pemutakhiran_ktp_id, $post_data, $lampiran_insert);
					if($check==true){
						$json_status = true;
						$json_mc = true;
						$json_msg = "Data Berhasil Disimpan";	
					}else{
						$json_status = false;
						$json_mc = false;
						$json_msg = "Data Gagal Disimpan";
					}
				}else{
						$json_status = false;
						$json_mc = false;
						$json_msg = "Data Gagal Disimpan";
				}
			}else{
				$check = $this->model->update($int_pemutakhiran_ktp_id, $post_data, null);
				if($check==true){
					$json_status = true;
					$json_mc = true;
					$json_msg = "Data Berhasil Disimpan";	
				}else{
					$json_status = false;
					$json_mc = false;
					$json_msg = "Data Gagal Disimpan";
				}
			}

			$this->set_json(array_merge([ 'stat' => $json_status, 
								'mc' => $json_mc,
								'msg' => $json_msg,
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							], $files));

		}
	}

	public function confirm($int_pemutakhiran_ktp_id){
		if($this->authCheckDetailAccess('d', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get_edit($int_pemutakhiran_ktp_id, false);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Error Detected.', 'message' => 'Data not found. ']], true);
		}else{
			$data['url']	= site_url("{$this->routeURL}/$int_pemutakhiran_ktp_id/del");
			$data['title']	= 'Hapus Data KTP';
			$data['info']   = ['No. Reg' => $res->var_reg_no,
								'NIK' => $res->var_nik,
                               'Nama' => $res->var_nama];
			$this->load_view('pemutakhiran/index_delete', $data);
		}
	}

	public function delete($int_mpemutakhiran_ktp_id){
		$this->authCheckDetailAccess('d');

		$check = $this->model->delete($int_mpemutakhiran_ktp_id);
		$this->set_json([  'stat' => $check, 
							'mc' => $check, //modal close
							'msg' => ($check)? "Data Deleted Successfully" : "Data Delete Failed",
							'csrf' => [ 'name' => $this->getCsrfName(),
										'token' => $this->getCsrfToken()]
						]);
	}

	public function confirm_del_lampiran($int_pemutakhiran_brm_img_id){
		if($this->authCheckDetailAccess('d', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get_lampiran_img($int_pemutakhiran_brm_img_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Image Not Found.', 'message' => '']], true);
		}else{
			$data['data'] 	= $res;
			$data['url']	= site_url("ver_brm/$int_pemutakhiran_brm_img_id/lampiran_del");
			$data['title']	= 'Delete Image';
			$this->load_view('ektp/index_delete_lampiran', $data);
		}
	}

	public function delete_lampiran($int_pemutakhiran_brm_img_id){
		$this->authCheckDetailAccess('d');

		$check = $this->model->delete_lampiran_img($int_pemutakhiran_brm_img_id);
		$this->set_json([  'stat' => $check, 
							'mc' => $check, //modal close
							'msg' => ($check)? "Data Deleted Successfully" : "Data Delete Failed",
							'csrf' => [ 'name' => $this->getCsrfName(),
										'token' => $this->getCsrfToken()]
						]);
	}	
}
