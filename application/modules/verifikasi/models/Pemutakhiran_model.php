<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Pemutakhiran_model extends MY_Model {

	public function get_agama(){
		$this->db->select("*")
		->from($this->m_agama);
		return $this->db->order_by('int_agama_id', 'ASC')->get()->result();
	}

	public function get_pendidikan(){
		$this->db->select("*")
		->from($this->m_pendidikan);

		return $this->db->order_by('int_pendidikan_id', 'ASC')->get()->result();
	}

	public function get_pekerjaan(){
		$this->db->select("*")
		->from($this->m_pekerjaan);

		return $this->db->order_by('int_pekerjaan_id', 'ASC')->get()->result();
	}

	public function get_status_kawin(){
		$this->db->select("*")
		->from($this->m_status_kawin);

		return $this->db->order_by('int_status_kawin_id', 'ASC')->get()->result();
	}

	public function get_kota_kab(){
		$this->db->select("*")
		->from($this->m_kota_kab);

		return $this->db->order_by('int_kota_kab_id', 'ASC')->get()->result();
	}

	public function list($periode_filter = "", $kecamatan_filter = 0, $kelurahan_filter = 0, $status_filter = "", $filter = NULL, $order_by = 0, $sort = 'ASC', $limit = 0, $ofset = 0){
		$this->db->select("mpk.*, tpk.int_pemutakhiran_ktp_id, tpk.created_at, mk.var_kecamatan, ml.var_kelurahan")
					->from($this->m_pemutakhiran_ktp." mpk")
					->join($this->t_pemutakhiran_ktp." tpk", "mpk.var_nik = tpk.var_nik", "left")
					->join($this->m_kecamatan." mk", "mpk.int_kecamatan_id = mk.int_kecamatan_id", "left")
					->join($this->m_kelurahan." ml", "(mpk.`int_kecamatan_id` = ml.`int_kecamatan_id` AND mpk.`int_kelurahan_id` = ml.`int_kelurahan_id`)", "left");

		if($periode_filter != ""){ // filter
			$this->db->where('mpk.int_periode_id', $periode_filter);
		}

		if($kecamatan_filter != 0){ // filter
			$this->db->where('mpk.int_kecamatan_id', $kecamatan_filter);
		}

		if($kelurahan_filter != 0){ // filter
			$this->db->where('mpk.int_kelurahan_id', $kelurahan_filter);
		}

		if($status_filter == 1){
			$this->db->where('tpk.int_pemutakhiran_ktp_id != ""');
		}else if($status_filter == 2){
			$this->db->where('tpk.int_pemutakhiran_ktp_id IS NULL');
		}


		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('mpk.var_nik', $filter)
					->or_like('mpk.var_nama', $filter)
					->or_like('mk.var_kecamatan', $filter)
					->or_like('ml.var_kelurahan', $filter)
					->group_end();
		}

		$order = 'mpk.var_reg_no, mpk.var_nik';
		switch($order_by){
			case 1 : $order = 'mpk.var_nik'; break;
			case 2 : $order = 'mpk.var_nama'; break;
			case 4 : $order = 'mk.var_kecamatan'; break;
			case 5 : $order = 'ml.var_kelurahan'; break;
		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($periode_filter = "", $kecamatan_filter = 0, $kelurahan_filter = 0, $status_filter = "", $filter = NULL){
		$this->db->select("*")
					->from($this->m_pemutakhiran_ktp." mpk")
					->join($this->t_pemutakhiran_ktp." tpk", "mpk.var_nik = tpk.var_nik", "left")
					->join($this->m_kecamatan." mk", "mpk.int_kecamatan_id = mk.int_kecamatan_id", "left")
					->join($this->m_kelurahan." ml", "(mpk.`int_kecamatan_id` = ml.`int_kecamatan_id` AND mpk.`int_kelurahan_id` = ml.`int_kelurahan_id`)", "left");

		if($periode_filter != ""){ // filter
			$this->db->where('mpk.int_periode_id', $periode_filter);
		}

		if($kecamatan_filter != 0){ // filter
			$this->db->where('mpk.int_kecamatan_id', $kecamatan_filter);
		}
			
		if($kelurahan_filter != 0){ // filter
			$this->db->where('mpk.int_kelurahan_id', $kelurahan_filter);
		}

		if($status_filter == 1){
			$this->db->where('tpk.int_pemutakhiran_ktp_id != ""');
		}else if($status_filter == 2){
			$this->db->where('tpk.int_pemutakhiran_ktp_id IS NULL');
		}

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
            $this->db->group_start()
			->like('mpk.var_nik', $filter)
			->or_like('mpk.var_nama', $filter)
			->or_like('mk.var_kecamatan', $filter)
			->or_like('ml.var_kelurahan', $filter)
			->group_end();
	    }
		return $this->db->count_all_results();
	}

	public function detail($int_mpemutakhiran_ktp_id){
		$data =  $this->db->select("mpk.*, tpk.`int_pemutakhiran_ktp_id` AS tpk_id, tpk.var_nohp, mj.var_jeniskelamin, mkc.`var_kecamatan`, mkl.`var_kelurahan`")
						->from($this->m_pemutakhiran_ktp." mpk")
						->join($this->t_pemutakhiran_ktp." tpk", "mpk.var_nik = tpk.var_nik", "left")
						->join($this->m_jeniskelamin." mj", "mpk.int_jeniskelamin_id = mj.int_jeniskelamin_id", "left")
						->join($this->m_kecamatan." mkc", "mpk.int_kecamatan_id = mkc.int_kecamatan_id", "left")
						->join($this->m_kelurahan." mkl", "(mpk.`int_kecamatan_id` = mkl.`int_kecamatan_id` AND mpk.`int_kelurahan_id` = mkl.`int_kelurahan_id`)", "left")
						->where('mpk.int_mpemutakhiran_ktp_id', $int_mpemutakhiran_ktp_id)->get()->row_array();

		return (object) $data;
	}
	
	public function detail_list($int_mpemutakhiran_ktp_id){
		$data =  $this->db->select("mpk.*, tpk.`int_pemutakhiran_ktp_id`, mj.var_jeniskelamin, mkc.`var_kecamatan`, mkl.`var_kelurahan`,
									mag.`var_agama`, mpn.`var_pendidikan`, mpj.`var_pekerjaan`, msk.`var_status_kawin`,
									tpk.`var_tempat_lahir` AS new_var_tempat_lahir,
									tpk.`dt_tanggal_lahir` AS new_dt_tanggal_lahir, nmag.`var_agama` AS new_var_agama,
									nmpn.`var_pendidikan` AS new_var_pendidikan,nmpj.`var_pekerjaan` AS new_var_pekerjaan,
									nmsk.`var_status_kawin` AS new_var_status_kawin, tpk.`var_nohp`")
						->from($this->m_pemutakhiran_ktp." mpk")
						->join($this->t_pemutakhiran_ktp." tpk", "(mpk.var_nik = tpk .var_nik AND mpk.int_periode_id = tpk.int_periode_id)", "left")

						->join($this->m_agama." mag", "mpk.int_agama_id = mag.int_agama_id", "left")
						->join($this->m_pendidikan." mpn", "mpk.int_pendidikan_id = mpn.int_pendidikan_id", "left")
						->join($this->m_pekerjaan." mpj", "mpk.int_pekerjaan_id = mpj.int_pekerjaan_id", "left")
						->join($this->m_status_kawin." msk", "mpk.int_status_kawin_id = msk.int_status_kawin_id", "left")

						->join($this->m_agama." nmag", "tpk.int_agama_id = nmag.int_agama_id", "left")
						->join($this->m_pendidikan." nmpn", "tpk.int_pendidikan_id = nmpn.int_pendidikan_id", "left")
						->join($this->m_pekerjaan." nmpj", "tpk.int_pekerjaan_id = nmpj.int_pekerjaan_id", "left")
						->join($this->m_status_kawin." nmsk", "tpk.int_status_kawin_id = nmsk.int_status_kawin_id", "left")

						->join($this->m_jeniskelamin." mj", "mpk.int_jeniskelamin_id = mj.int_jeniskelamin_id", "left")
						->join($this->m_kecamatan." mkc", "mpk.int_kecamatan_id = mkc.int_kecamatan_id", "left")
						->join($this->m_kelurahan." mkl", "(mpk.`int_kecamatan_id` = mkl.`int_kecamatan_id` AND mpk.`int_kelurahan_id` = mkl.`int_kelurahan_id`)", "left")
						->where('mpk.int_mpemutakhiran_ktp_id', $int_mpemutakhiran_ktp_id)->get()->row_array();

		return (object) $data;
	}

	public function add_form($int_mpemutakhiran_ktp_id){
		$data =  $this->db->select("var_reg_no, var_nik, var_nama, int_periode_id")
						->from($this->m_pemutakhiran_ktp)
						->where('int_mpemutakhiran_ktp_id', $int_mpemutakhiran_ktp_id)->get()->row_array();

		return (object) $data;
	}
				

	public function create($ins, $lampiran = []){
		if(isset($ins['var_jenis_img'])){
			$var_jenis_img = $ins['var_jenis_img'];
		}else{
			$var_jenis_img = '';
		}
		$ins['created_at'] = date("Y-m-d H:i:s");
		$ins['created_by'] = $this->session->userdata['user_id'];
		$this->db->trans_begin();
		$ins = $this->clearFormInsert($ins, ['lampiran', 'var_jenis_img', 'img_select', 'sidaku_token', 'cdImg']);

		$this->db->insert($this->t_pemutakhiran_ktp, $ins);
		$int_pemutakhiran_ktp_id = $this->db->insert_id();
		$this->setQueryLog('ins_posts');

		if(!empty($lampiran)){
			$lampiran_insert = [];
			foreach($lampiran as $lmp){
				$lampiran_insert[] = ['int_pemutakhiran_ktp_id' => $int_pemutakhiran_ktp_id,
									'var_image' => $this->db->escape($lmp['dir'].'/'.$lmp['file_name']),
									'var_jenis_img' => $this->db->escape($var_jenis_img)];
			}
			$this->db->insert_batch($this->t_pemutakhiran_ktp_img, $lampiran_insert, false);
			$this->setQueryLog('ins_lampiran');
		}

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}	

	public function get_edit($int_pemutakhiran_ktp_id, $with_lampiran = true){
		$data =  $this->db->select("*")
						->from($this->t_pemutakhiran_ktp)
						->where('int_pemutakhiran_ktp_id', $int_pemutakhiran_ktp_id)->get()->row_array();

		if(!empty($data)){
			$lampiran = ($with_lampiran)? $this->get_lampiran($int_pemutakhiran_ktp_id) : null;
		}

		return (object) array_merge($data, ['lampiran' => $lampiran]);
	}
				
	public function get_lampiran($int_pemutakhiran_ktp_id){
		return $this->db->query("SELECT *
								FROM  {$this->t_pemutakhiran_ktp_img} 
								WHERE int_pemutakhiran_ktp_id = {$int_pemutakhiran_ktp_id}")->result();
			
	}

	public function update($int_pemutakhiran_ktp_id, $upd, $lampiran = []){
		if(isset($upd['var_jenis_img'])){
			$var_jenis_img = $upd['var_jenis_img'];
		}else{
			$var_jenis_img = '';
		}

		$upd['updated_at'] = date("Y-m-d H:i:s");
		$upd['updated_by'] = $this->session->userdata['user_id'];
		$this->db->trans_begin();
		$upd = $this->clearFormInsert($upd, ['lampiran', 'var_jenis_img', 'img_select', 'sidaku_token', 'cdImg']);

		$this->db->where('int_pemutakhiran_ktp_id', $int_pemutakhiran_ktp_id);
		$this->db->update($this->t_pemutakhiran_ktp, $upd);
		$this->setQueryLog('upd_brm');
		
		if(!empty($lampiran)){
			$lampiran_insert = [];
			foreach($lampiran as $lmp){
				$lampiran_insert[] = ['int_pemutakhiran_ktp_id' => $int_pemutakhiran_ktp_id,
									'var_image' => $this->db->escape($lmp['dir'].'/'.$lmp['file_name']),
									'var_jenis_img' => $this->db->escape($var_jenis_img)];
			}
			$this->db->insert_batch($this->t_pemutakhiran_ktp_img, $lampiran_insert, false);
			$this->setQueryLog('ins_lampiran');
		}
				
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function delete($int_pemutakhiran_ktp_id){
		$this->db->trans_begin();
		$this->db->delete($this->t_pemutakhiran_ktp,  ['int_pemutakhiran_ktp_id' => $int_pemutakhiran_ktp_id]);
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function get_lampiran_img($int_pemutakhiran_ktp_img_id){
		return $this->db->query("SELECT *
								FROM 	{$this->t_pemutakhiran_ktp_img} 
								WHERE 	int_pemutakhiran_ktp_img_id = {$int_pemutakhiran_ktp_img_id}")->row();
	}
	
	public function delete_lampiran_img($int_pemutakhiran_ktp_img_id){
		$data = $this->get_lampiran_img($int_pemutakhiran_ktp_img_id);
		if(!empty($data)){
			//$upload_path= $this->config->item('upload_path');
			//$img_dir = $this->config->item('img_dir');
			//$unlink_dir = str_replace($img_dir,"",$upload_path);
			unlink(FCPATH.'/'.$data->var_image);
			
			$this->db->trans_begin();
			$this->db->query("DELETE FROM {$this->t_pemutakhiran_ktp_img} WHERE int_pemutakhiran_ktp_img_id = {$int_pemutakhiran_ktp_img_id}");
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return false;
			}else{
				$this->db->trans_commit();
				return true;
			}
		}else{
			return false;
		}
	}
}
