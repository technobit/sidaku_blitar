<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App extends MX_Controller {
    private $mode = ['Hari Ini', 'Minggu Ini', 'Bulan Ini'];
	function __construct(){
        parent::__construct();
		
		$this->kodeMenu = 'APP-HOME';
		$this->authCheck();
		
		$this->load->model('app_model', 'model');
    }
	
	public function index(){
		$this->page->subtitle = 'Dashboard';
		$this->page->menu = 'dashboard';
        $this->breadcrumb->title = 'Dashboard';;
        $this->breadcrumb->list = [getServerDate(true)];
		$this->js = true;
        $data['total_brm']   = $this->model->get_total(1);
        $data['total_bck']   = $this->model->get_total(2);
        $data['total_kba']   = $this->model->get_total(3);
        $data['verif_brm']   = $this->model->get_verif('brm');
        $data['verif_bck']   = $this->model->get_verif('bck');
        $data['verif_kba']   = $this->model->get_verif('kba');
		$this->render_view('index', $data, false);
	}

}
