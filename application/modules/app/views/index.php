<div class="container-fluid">

        <div class="row">
          <div class="col-md-12">
            <div class="card card-danger">
              <div class="card-header">
                <h3 class="card-title">Proses Verifikasi Data KTP</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                </div>
              </div>
              <div class="card-body">

                <div class="row">
                    <div class="col-md-4 col-12">
                        <div class="info-box bg-gradient-danger">
                        <span class="info-box-icon"><i class="fas fa-camera-retro"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Belum Perekaman</span>
                            <span class="info-box-number"><?=number_format($verif_brm,0, ',', '.' )?> Data Terverifikasi</span>

                            <div class="progress">
                            <div class="progress-bar" style="width: <?=$verif_brm/$total_brm*100?>%"></div>
                            </div>
                            <span class="progress-description">
                            <?=number_format($verif_brm/$total_brm*100,2, ',', '.' )?>% Dari Total <?=number_format($total_brm,0, ',', '.' )?> Data
                            </span>
                        </div>
                        <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-4 col-12">
                        <div class="info-box bg-gradient-warning">
                        <span class="info-box-icon"><i class="fas fa-print"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Belum Cetak</span>
                            <span class="info-box-number"><?=number_format($verif_bck,0, ',', '.' )?> Data Terverifikasi</span>

                            <div class="progress">
                            <div class="progress-bar" style="width: <?=$verif_bck/$total_bck*100?>%"></div>
                            </div>
                            <span class="progress-description">
                            <?=number_format($verif_bck/$total_bck*100,2, ',', '.' )?>% Dari Total <?=number_format($total_bck,0, ',', '.' )?> Data
                            </span>
                        </div>
                        <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-4 col-12">
                        <div class="info-box bg-gradient-primary">
                        <span class="info-box-icon"><i class="fas fa-hand-holding"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Belum Ambil</span>
                            <span class="info-box-number"><?=number_format($verif_kba,0, ',', '.' )?> Data Terverifikasi</span>

                            <div class="progress">
                            <div class="progress-bar" style="width: <?=$verif_kba/$total_kba*100?>%"></div>
                            </div>
                            <span class="progress-description">
                            <?=number_format($verif_kba/$total_kba*100,2, ',', '.' )?>% Dari Total <?=number_format($total_kba,0, ',', '.' )?> Data
                            </span>
                        </div>
                        <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->

                </div>
               </div>
                <!-- /.card-body -->
            </div>
          </div>
</div>
<div id="ajax-modal" class="modal fade animate shake" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="75%" aria-hidden="true"></div>