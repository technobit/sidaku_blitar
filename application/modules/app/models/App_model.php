<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class App_model extends MY_Model {

    public function get_total($int_kategori_ktp_id){
        $this->db->from($this->m_verifikasi_ktp." tvb")
                ->where('int_kategori_ktp_id', $int_kategori_ktp_id);

     	return $this->db->count_all_results();

    }

    public function get_verif($table){
        $this->db->from("t_verifikasi_".$table."")
                ->group_by('var_nik');

     	return $this->db->count_all_results();
    }
}