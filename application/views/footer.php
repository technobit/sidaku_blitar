<footer class="main-footer">
    <strong>Copyright &copy; 2020 <a href="http://diskominfotik.blitarkota.go.id/">Dinas Komunikasi Informatika dan Statistik. Kota Blitar</a>.</strong> All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
    Page rendered in <strong>{elapsed_time}</strong> s. <b>Version</b> 2.0.1
    </div>
</footer>